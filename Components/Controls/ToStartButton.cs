﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace Components.Controls
{
    public partial class ToStartButton : UserControl
    {
        public Form DestinationForm { get; set; }
        public Form SourceForm { get; set; }
        public ToStartButton()
        {
            this.InitializeComponent();
            BorderStyle = BorderStyle.Fixed3D;
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            
            int left = ClientRectangle.Left + 4;
            int right = ClientRectangle.Right - 4;
            int top = ClientRectangle.Top + 4;
            int bottom = ClientRectangle.Bottom - 4;
            Pen pen = new Pen(ForeColor, 2);
            e.Graphics.DrawLine(pen,left,top,right,bottom);
            e.Graphics.DrawLine(pen, left, bottom, right, top);
        }

        private void ToStartButton_Click(object sender, EventArgs e)
        {
            SourceForm?.Hide();
            DestinationForm?.Show();
        }
    }
}
