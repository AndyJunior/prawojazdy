﻿using System;
using System.Diagnostics;
using System.Windows.Forms;

namespace Components.Controls
{
    public partial class CountDownTimer : UserControl, IDisposable
    {
        private Timer _timer;
        private Stopwatch _stpWatch;
        private TimeSpan _max;
        public Action TimeChanged;
        public Action CountDownFinished;
        public bool IsRunning => _timer.Enabled;
        public int StepMs
        {
            get => _timer.Interval;
            set => _timer.Interval = value;
        }

        public TimeSpan SetTimer
        {
            get => _max;
            set => _max = value;
        }
        public TimeSpan TimeLeft => (_max.TotalMilliseconds - _stpWatch.ElapsedMilliseconds) > 0 ?
            TimeSpan.FromMilliseconds(Math.Round((_max.TotalMilliseconds - _stpWatch.ElapsedMilliseconds) / 1000) * 1000) : TimeSpan.FromMilliseconds(0);

        private bool MustStop => (_max.TotalMilliseconds - _stpWatch.ElapsedMilliseconds) < 0;


        public string TimeLeftStr => TimeLeft.ToString(@"mm\:ss");
        public CountDownTimer()
        {
            InitializeComponent();
            this.Init();
            TimeChanged += () => label1.Text = TimeLeftStr;
        }
        private void TimerTick(object sender, EventArgs e)
        {
            TimeChanged?.Invoke();

            if (MustStop)
            {
                this.Stop();
                CountDownFinished?.Invoke();
                _timer.Enabled = false;
            }
        }
        private void Init()
        {
            _timer = new Timer();
            _stpWatch = new Stopwatch();
            StepMs = 1000;
            _timer.Tick += new EventHandler(TimerTick);
        }

        public void SetTime(TimeSpan ts)
        {
            _max = ts;
            TimeChanged?.Invoke();
        }

        public void SetTime(int min, int sec = 0) => SetTime(TimeSpan.FromSeconds(min * 60 + sec));

        public void Start()
        {
            _timer.Start();
            _stpWatch.Start();
        }
        public void Pause()
        {
            _timer.Stop();
            _stpWatch.Stop();
        }

        public void Stop()
        {
            this.Reset();
            this.Pause();
        }

        public void Reset()
        {
            _stpWatch.Reset();
        }

        public void Restart()
        {
            _timer.Enabled = true;
            _timer.Stop();
            _timer.Start();
            _stpWatch.Restart();
            
        }

        public new void Dispose() => _timer.Dispose();
    }
}
