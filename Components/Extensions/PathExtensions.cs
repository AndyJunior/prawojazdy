﻿using System.IO;

namespace Components.Extensions
{
    public static class PathExtension
    {
        public static string GetPathToSolutionFromFormsFolder() =>
        Path.GetDirectoryName(Path.GetDirectoryName(Path.GetDirectoryName(System.IO.Directory.GetCurrentDirectory())));
        public static string GetFullPathToResources() =>
            Path.Combine(GetPathToSolutionFromFormsFolder(), "Database", "Resources");
    }
}
