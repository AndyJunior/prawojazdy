﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using Components.Extensions;
using Database.Model;
using Database.Services;


namespace Components.Components
{
    public partial class QuestionRandomizer : Component
    {
        private IList<Question> _questions;
        public int EasyCounter { get; set; }
        public int HardCounter { get; set; }
        public IList<Question> Questions {
            get
            {
                if (IsRandom)
                {
                    _questions.Shuffle();
                }

                return _questions;
            }
        }
        
        public bool IsRandom { get; set; }
        public bool IsLimit { get; set; }

        public QuestionRandomizer()
        {
            this.InitializeComponent();
            this.InitializeQuestions();
        }

        public QuestionRandomizer(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }


        public void InitializeQuestions(int easy = 0, int hard = 0)
        {
            _questions = new List<Question>();
            if (easy == 0 || hard == 0)
            {
                InitQuestions(EasyCounter, DatabaseService.GetEasyQuestions().ToList());
                InitQuestions(HardCounter, DatabaseService.GetHardQuestions().ToList());
            }
            else
            {
                InitQuestions(easy, DatabaseService.GetEasyQuestions().ToList());
                InitQuestions(hard, DatabaseService.GetHardQuestions().ToList());
            }
           
            
        }

        private void InitQuestions(int counter, List<Question> questions)
        {
            if (counter > questions.Count)
            {
                counter = questions.Count;
            }

            if (IsLimit)
            {
                questions.Shuffle();
                for (int i = 0; i < counter; i++)
                {
                    _questions.Add(questions[i]);
                }
            }
            else
            {
                foreach (var question in questions)
                {
                    _questions.Add(question);
                }
            }
        }
    }
    
}
