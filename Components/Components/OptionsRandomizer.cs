﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Components.Extensions;
using Database.Model;
using Database.Services;

namespace Components.Components
{
    public partial class OptionsRandomizer : Component
    {
        public IList<QuestionOption> QuestionOptions { get; set; }
        public OptionsRandomizer()
        {
            this.InitializeComponent();
        }
        public OptionsRandomizer(Question question)
        {
            this.InitializeComponent();
            this.InitializeQuestionOptions(question);
        }

        public OptionsRandomizer(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        public void InitializeQuestionOptions(Question question)
        {
            QuestionOptions = new List<QuestionOption>();

            var query = GetRandomizedQuestionOptions(question);
            foreach (var questionOption in query)
            {
                QuestionOptions.Add(questionOption);
            }

        }

        private List<QuestionOption> GetRandomizedQuestionOptions(Question question)
        {
            var randomizedQuestionOptions = DatabaseService.GetQuestionOptions(question.Id).ToList();
            randomizedQuestionOptions.Shuffle();
            return randomizedQuestionOptions;
        }
    }
}
