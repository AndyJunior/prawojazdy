﻿using System;
using System.Windows.Forms;

namespace PrawoJazdy.Forms
{
    public partial class AboutForm : Form
    {
        public AboutForm()
        {
            InitializeComponent();
        }

        private void toStartButton1_Load(object sender, EventArgs e)
        {
            toStartButton1.SourceForm = this;
            toStartButton1.DestinationForm = new MainForm();
        }
    }
}
