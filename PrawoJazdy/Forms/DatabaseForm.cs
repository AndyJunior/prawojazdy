﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PrawoJazdy.Forms
{
    public partial class DatabaseForm : Form
    {
        public DatabaseForm()
        {
            InitializeComponent();
        }

        private void toStartButton1_Load(object sender, EventArgs e)
        {
            toStartButton1.DestinationForm = new MainForm();
            toStartButton1.SourceForm = this;
        }

        private void DatabaseForm_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'prawoJazdyDataSet.Questions' table. You can move, or remove it, as needed.
            this.questionsTableAdapter.Fill(this.prawoJazdyDataSet.Questions);

        }
    }
}
