﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using Components.Components;
using Components.Extensions;
using Database.Model;
using Database.Services;
using PrawoJazdy.Forms;

namespace PrawoJazdy
{
    public partial class QuizForm : Form
    {
        public IList<Question> Questions { get; set; }

        private Question _currentQuestion;
        private IList<QuestionOption> _questionOptions;
        private QuestionOption _chosenOption;
        private IList<Question> _easyQuestions;
        private IList<Question> _hardQuestions;
        private Dictionary<RadioButton, QuestionOption> _radioButtonQuestionOptions;
        private int _questionCounter;
        private bool _isHard;
        public QuizForm()
        {
            InitializeComponent();
        }

        public QuizForm(IList<Question> questions)
        {
            InitializeComponent();

            Init(questions);
            NextQuestion(false);
        }

        private void NextQuestion(bool isHard)
        {
            ProgressLabel.Text = string.Empty;
            QuestionOptionRadio1.Checked = true;
            
            if (_isHard)
            {
                NextQuestion(isHard,_hardQuestions.ToList());
            }
            else
            {
                NextQuestion(isHard,_easyQuestions.ToList());
            }
            _questionCounter++;
        }

        private void NextQuestion(bool isHard,List<Question> questions)
        {
            _currentQuestion = questions[_questionCounter];
            ProgressLabel.Text = "Pytanie " + (_questionCounter + 1) + " / " + questions.Count;
            OptionsRandomizer = new OptionsRandomizer(_currentQuestion);
            _questionOptions = OptionsRandomizer.QuestionOptions;
            if (isHard)
            {
                QuestionOptionRadio3.Text = "C. " + _questionOptions[2].Text;
                RefreshQuiz(true);
            }
            else
            {
                RefreshQuiz(false);
            }
        }

        private void RefreshQuiz(bool isHard)
        {
            QuestionOptionRadio3.Enabled = isHard;
            QuestionOptionRadio3.Visible = isHard;
            ChangeControls();
            Refresh();
        }

        private void ChangeControls()
        {
            QuestionLabel.Text = _currentQuestion.Text;
            SetPicture();
            QuestionOptionRadio1.Text = "A. " + _questionOptions[0].Text;
            QuestionOptionRadio2.Text = "B. " + _questionOptions[1].Text;
        }

        private void SubmitButton_Click(object sender, EventArgs e)
        {
            if (_isHard)
            {
                Submit(_isHard, _hardQuestions.ToList());
            }
            else
            {
                Submit(_isHard, _easyQuestions.ToList());
            }
        }

        private void Submit(bool isHard, List<Question> questions)
        {
            if (_questionCounter < questions.Count)
            {
                GetCheckedRadioButton();
                Answer();
                NextQuestion(isHard);
            }
            else
            {
                Answer();
                if (!isHard)
                {
                    _isHard = true;
                    _questionCounter = 0;
                    NextQuestion(true);
                }
                else
                {
                    countDownTimer1.Stop();
                    CreateResultForm(true);
                }
            }
        }

        private void Answer()
        {
            _radioButtonQuestionOptions = new Dictionary<RadioButton, QuestionOption>()
            {
                [QuestionOptionRadio1] = _questionOptions[0],
                [QuestionOptionRadio2] = _questionOptions[1],
                
            };
            if (_isHard)
            {
                _radioButtonQuestionOptions[QuestionOptionRadio3] = _questionOptions[2];
            }
            
            _chosenOption = _radioButtonQuestionOptions[GetCheckedRadioButton()];
            DatabaseService.CreateAnswer(_currentQuestion, _chosenOption);
        }

        private void NoAnswer()
        {
            DatabaseService.CreateAnswer(_currentQuestion, null);
            MessageBox.Show("Egzamin został zakończony");
            if (!_isHard)
            {
                for (int i = _questionCounter; i < _easyQuestions.Count; i++)
                {
                    DatabaseService.CreateAnswer(Questions[i], null);
                }
                for (int i = 0; i < _hardQuestions.Count; i++)
                {
                    DatabaseService.CreateAnswer(Questions[i], null);
                }
            }
            else
            {
                for (int i = _questionCounter; i < _hardQuestions.Count; i++)
                {
                    DatabaseService.CreateAnswer(Questions[i], null);
                }
            }
            countDownTimer1.Stop();
            CreateResultForm(true);
        }

        private void SetPicture()
        {
            pictureBox.Image = Image.FromFile(_currentQuestion.PhotoPath != null ? Path.Combine(PathExtension.GetFullPathToResources(), 
                _currentQuestion.PhotoPath) : Path.Combine(PathExtension.GetFullPathToResources(), "default.png"));
        }
        private RadioButton GetCheckedRadioButton()
        {
            return groupBox1.Controls.OfType<RadioButton>()
                .FirstOrDefault(rb => rb.Checked);
        }

        private void Init(IList<Question> questions)
        {
            Questions = questions;
            _easyQuestions = questions.Where(q => q.IsHard == false).ToList();
            _hardQuestions = questions.Where(q => q.IsHard).ToList();
            countDownTimer1.Start();
            countDownTimer1.CountDownFinished += NoAnswer;


        }
        private void CreateResultForm(bool isExam)
        {
            this.Hide();
            new ResultForm(isExam).Show();
        }

        private void toStartButton1_Click(object sender, EventArgs e)
        {
            var confirm = new ConfirmationForm();
            if (confirm.ShowDialog() == DialogResult.OK)
            {
                this.countDownTimer1.Stop();
                this.Hide();
                new MainForm().Show();
            }
        }
    }
}
