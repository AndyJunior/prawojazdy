﻿using System;
using System.Windows.Forms;
using Database.Services;
using PrawoJazdy.Forms;

namespace PrawoJazdy
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void StartButton_Click(object sender, EventArgs e)
        {
            Randomizer.IsLimit = true;
            Randomizer.InitializeQuestions();
            DatabaseService.RemoveAnswers();
            this.Hide();
            new QuizForm(Randomizer.Questions).Show();
        }

        private void LearnButton_Click(object sender, EventArgs e)
        {
            DatabaseService.RemoveAnswers();
            
            new ConfigStudyForm(this).Show();
            
        }

        private void toolStripMenuItem2_Click(object sender, EventArgs e)
        {
            this.Hide();
            new AboutForm().Show();
        }


        private void dodajToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PasswordForm frm = new PasswordForm();
            if (frm.ShowDialog() == DialogResult.OK)
            {
                this.Hide();
                new DatabaseForm().Show();
            }
        }

    }
}
