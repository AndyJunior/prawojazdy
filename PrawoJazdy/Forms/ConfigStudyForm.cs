﻿using System;
using System.Windows.Forms;
using Database.Services;

namespace PrawoJazdy.Forms
{
    public partial class ConfigStudyForm : Form
    {
        private readonly MainForm _mainForm;
        private bool _isAnswerShown;
        public ConfigStudyForm()
        {
            InitializeComponent();
            
        }
        public ConfigStudyForm(MainForm mf)
        {
            InitializeComponent();
            _mainForm = mf;

            InitConfig();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            questionRandomizer1.InitializeQuestions(Convert.ToInt32(numericUpDown1.Value), Convert.ToInt32(numericUpDown2.Value));
            _mainForm.Hide();
            this.Hide();
            new StudyForm(questionRandomizer1.Questions, _isAnswerShown).Show();
        }
        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            _isAnswerShown = checkBox1.Checked;
        }

        private void InitConfig()
        {
            questionRandomizer1.IsLimit = true;

            numericUpDown1.Minimum = 1;
            numericUpDown1.Maximum = DatabaseService.GetEasyQuestionsCount();
            numericUpDown1.Value = numericUpDown1.Maximum;
            numericUpDown2.Minimum = 1;
            numericUpDown2.Maximum = DatabaseService.GetHardQuestionsCount();
            numericUpDown2.Value = numericUpDown2.Maximum;
        }
        
    }
}
