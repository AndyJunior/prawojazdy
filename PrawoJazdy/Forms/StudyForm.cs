﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Components.Components;
using Components.Extensions;
using Database.Model;
using Database.Services;

namespace PrawoJazdy.Forms
{
    public partial class StudyForm : Form
    {
        public IList<Question> Questions { get; set; }
        private Question _currentQuestion;
        private IList<QuestionOption> _questionOptions;
        private IList<Question> _easyQuestions;
        private IList<Question> _hardQuestions;
        private QuestionOption _chosenOption;
        private Dictionary<RadioButton, QuestionOption> _radioButtonQuestionOptions;
        private QuestionOption _correctOption;
        private bool _isHard;
        private bool _isAnswerShown;
        private int _questionCounter;
        public StudyForm()
        {
            InitializeComponent();
        }

        public StudyForm(IList<Question> questions, bool isAnswerShown = false)
        {
            _isAnswerShown = isAnswerShown;
            
            InitializeComponent();
            if (_isAnswerShown)
            {
                button1.Hide();
            }
            Init(questions);
            NextQuestion(false);
            
            _radioButtonQuestionOptions = new Dictionary<RadioButton, QuestionOption>()
            {
                [QuestionOptionRadio1] = _questionOptions[0],
                [QuestionOptionRadio2] = _questionOptions[1],

            };
            if (_isHard)
            {
                _radioButtonQuestionOptions[QuestionOptionRadio3] = _questionOptions[2];
            }
        }
        private void Init(IList<Question> questions)
        {
            Questions = questions;
            _easyQuestions = questions.Where(q => q.IsHard == false).ToList();
            _hardQuestions = questions.Where(q => q.IsHard).ToList();
        }
        private void NextQuestion(bool isHard)
        {
            
            ProgressLabel.Text = string.Empty;
            QuestionOptionRadio1.Checked = true;
              if (_isHard)
            {
                NextQuestion(isHard, _hardQuestions.ToList());
            }
            else
            {
                NextQuestion(isHard, _easyQuestions.ToList());
            }
            _questionCounter++;
        }
        private void NextQuestion(bool isHard, List<Question> questions)
        {

            _currentQuestion = questions[_questionCounter];
            ProgressLabel.Text = "Pytanie " + (_questionCounter + 1) + " / " + questions.Count;
            OptionsRandomizer = new OptionsRandomizer(_currentQuestion);
            _questionOptions = OptionsRandomizer.QuestionOptions;
            _radioButtonQuestionOptions = new Dictionary<RadioButton, QuestionOption>()
            {
                [QuestionOptionRadio1] = _questionOptions[0],
                [QuestionOptionRadio2] = _questionOptions[1],

            };
            if (_isHard)
            {
                _radioButtonQuestionOptions[QuestionOptionRadio3] = _questionOptions[2];
            }
            if (isHard)
            {
                QuestionOptionRadio3.Text = "C. " + _questionOptions[2].Text;
                RefreshQuiz(isHard);
            }
            else
            {
                RefreshQuiz(isHard);
            }
        }
        private void RefreshQuiz(bool isHard)
        {
            QuestionOptionRadio3.Enabled = isHard;
            QuestionOptionRadio3.Visible = isHard;
            ChangeControls();
            Refresh();
        }
        private void ChangeControls()
        {
            QuestionLabel.Text = _currentQuestion.Text;
            SetPicture();
            QuestionOptionRadio1.Text ="A. " + _questionOptions[0].Text;
            QuestionOptionRadio2.Text = "B. " + _questionOptions[1].Text;
        }
        private void SetPicture()
        {
            pictureBox.Image = Image.FromFile(_currentQuestion.PhotoPath != null ? Path.Combine(PathExtension.GetFullPathToResources(),
                _currentQuestion.PhotoPath) : Path.Combine(PathExtension.GetFullPathToResources(), "default.png"));
        }

        private void toStartButton1_Load(object sender, EventArgs e)
        {
            toStartButton1.DestinationForm = new MainForm();
            toStartButton1.SourceForm = this;
        }
        private RadioButton GetCheckedRadioButton()
        {
            return groupBox1.Controls.OfType<RadioButton>()
                .FirstOrDefault(rb => rb.Checked);
        }
        private void Submit(bool isHard, List<Question> questions)
        {

            ClearOptions();
            if (_questionCounter < questions.Count)
            {
                GetCheckedRadioButton();
                Answer();
                NextQuestion(isHard);
                
            }
            else
            {

                Answer();
                if (!isHard)
                {
 
                    _isHard = true;
                    _questionCounter = 0;
                    NextQuestion(true);
                }
                else
                {
                    SubmitButton.Enabled = false;
                    CreateResultForm();
                }
            }
        }
        private void Answer()
        {
            
            _chosenOption = _radioButtonQuestionOptions[GetCheckedRadioButton()];
            DatabaseService.CreateAnswer(_currentQuestion, _chosenOption);
        }
        private void CreateResultForm()
        {
            this.Hide();
            new ResultForm(false).Show();
        }

        private void SubmitButton_Click_1(object sender, EventArgs e)
        {
            if (_isHard)
            {
                Submit(_isHard, _hardQuestions.ToList());
            }
            else
            {
                Submit(_isHard, _easyQuestions.ToList());
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            _correctOption = DatabaseService.GetCorrectOption(_currentQuestion.Id);
            var q = _radioButtonQuestionOptions.FirstOrDefault(r => r.Value == _correctOption);
            q.Key.BackColor = Color.GreenYellow;
            Console.WriteLine(DatabaseService.GetCorrectDescription(_currentQuestion.Id));
            toolTip1.SetToolTip(q.Key, DatabaseService.GetCorrectDescription(_currentQuestion.Id));

        }

        private void ClearOptions()
        {
            toolTip1.RemoveAll();
            QuestionOptionRadio1.BackColor = Color.FromName("Control");
            QuestionOptionRadio2.BackColor = Color.FromName("Control");
            QuestionOptionRadio3.BackColor = Color.FromName("Control");

        }

        private void toolTip1_Popup(object sender, PopupEventArgs e)
        {
            e.ToolTipSize = TextRenderer.MeasureText(DatabaseService.GetCorrectDescription(_currentQuestion.Id), new Font("Arial", 20.0f));
        }
        
    }

}
