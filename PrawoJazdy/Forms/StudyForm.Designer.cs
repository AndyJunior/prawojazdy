﻿
namespace PrawoJazdy.Forms
{
    partial class StudyForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.pictureBox = new System.Windows.Forms.PictureBox();
            this.ProgressLabel = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.button1 = new System.Windows.Forms.Button();
            this.SubmitButton = new System.Windows.Forms.Button();
            this.QuestionOptionRadio3 = new System.Windows.Forms.RadioButton();
            this.QuestionOptionRadio2 = new System.Windows.Forms.RadioButton();
            this.QuestionOptionRadio1 = new System.Windows.Forms.RadioButton();
            this.QuestionLabel = new System.Windows.Forms.RichTextBox();
            this.toStartButton1 = new Components.Controls.ToStartButton();
            this.QuestionRandomizer = new Components.Components.QuestionRandomizer(this.components);
            this.OptionsRandomizer = new Components.Components.OptionsRandomizer(this.components);
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictureBox
            // 
            this.pictureBox.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox.Location = new System.Drawing.Point(5, 21);
            this.pictureBox.Name = "pictureBox";
            this.pictureBox.Size = new System.Drawing.Size(783, 251);
            this.pictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox.TabIndex = 8;
            this.pictureBox.TabStop = false;
            // 
            // ProgressLabel
            // 
            this.ProgressLabel.AutoSize = true;
            this.ProgressLabel.Font = new System.Drawing.Font("Roboto", 12.25F, System.Drawing.FontStyle.Bold);
            this.ProgressLabel.Location = new System.Drawing.Point(341, 275);
            this.ProgressLabel.Name = "ProgressLabel";
            this.ProgressLabel.Size = new System.Drawing.Size(62, 23);
            this.ProgressLabel.TabIndex = 13;
            this.ProgressLabel.Text = "label1";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.SubmitButton);
            this.groupBox1.Controls.Add(this.QuestionOptionRadio3);
            this.groupBox1.Controls.Add(this.QuestionOptionRadio2);
            this.groupBox1.Controls.Add(this.QuestionOptionRadio1);
            this.groupBox1.Font = new System.Drawing.Font("Roboto", 12.25F);
            this.groupBox1.Location = new System.Drawing.Point(12, 367);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(780, 279);
            this.groupBox1.TabIndex = 14;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Odpowiedzi";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(0, 220);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(154, 56);
            this.button1.TabIndex = 5;
            this.button1.Text = "Pokaż odpowiedź";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // SubmitButton
            // 
            this.SubmitButton.Location = new System.Drawing.Point(626, 220);
            this.SubmitButton.Name = "SubmitButton";
            this.SubmitButton.Size = new System.Drawing.Size(154, 56);
            this.SubmitButton.TabIndex = 4;
            this.SubmitButton.Text = "Następne pytanie";
            this.SubmitButton.UseVisualStyleBackColor = true;
            this.SubmitButton.Click += new System.EventHandler(this.SubmitButton_Click_1);
            // 
            // QuestionOptionRadio3
            // 
            this.QuestionOptionRadio3.AutoSize = true;
            this.QuestionOptionRadio3.Location = new System.Drawing.Point(24, 118);
            this.QuestionOptionRadio3.Name = "QuestionOptionRadio3";
            this.QuestionOptionRadio3.Size = new System.Drawing.Size(128, 27);
            this.QuestionOptionRadio3.TabIndex = 2;
            this.QuestionOptionRadio3.TabStop = true;
            this.QuestionOptionRadio3.Text = "radioButton3";
            this.QuestionOptionRadio3.UseVisualStyleBackColor = true;
            // 
            // QuestionOptionRadio2
            // 
            this.QuestionOptionRadio2.AutoSize = true;
            this.QuestionOptionRadio2.Location = new System.Drawing.Point(24, 79);
            this.QuestionOptionRadio2.Name = "QuestionOptionRadio2";
            this.QuestionOptionRadio2.Size = new System.Drawing.Size(128, 27);
            this.QuestionOptionRadio2.TabIndex = 1;
            this.QuestionOptionRadio2.TabStop = true;
            this.QuestionOptionRadio2.Text = "radioButton2";
            this.QuestionOptionRadio2.UseVisualStyleBackColor = true;
            // 
            // QuestionOptionRadio1
            // 
            this.QuestionOptionRadio1.AutoSize = true;
            this.QuestionOptionRadio1.Location = new System.Drawing.Point(24, 39);
            this.QuestionOptionRadio1.Name = "QuestionOptionRadio1";
            this.QuestionOptionRadio1.Size = new System.Drawing.Size(128, 27);
            this.QuestionOptionRadio1.TabIndex = 0;
            this.QuestionOptionRadio1.TabStop = true;
            this.QuestionOptionRadio1.Text = "radioButton1";
            this.QuestionOptionRadio1.UseVisualStyleBackColor = true;
            // 
            // QuestionLabel
            // 
            this.QuestionLabel.BackColor = System.Drawing.SystemColors.Control;
            this.QuestionLabel.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.QuestionLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.QuestionLabel.HideSelection = false;
            this.QuestionLabel.ImeMode = System.Windows.Forms.ImeMode.On;
            this.QuestionLabel.Location = new System.Drawing.Point(9, 297);
            this.QuestionLabel.Name = "QuestionLabel";
            this.QuestionLabel.ReadOnly = true;
            this.QuestionLabel.Size = new System.Drawing.Size(779, 64);
            this.QuestionLabel.TabIndex = 12;
            this.QuestionLabel.Text = "Pytanie";
            // 
            // toStartButton1
            // 
            this.toStartButton1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.toStartButton1.DestinationForm = null;
            this.toStartButton1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.toStartButton1.ForeColor = System.Drawing.Color.Red;
            this.toStartButton1.Location = new System.Drawing.Point(756, 21);
            this.toStartButton1.Name = "toStartButton1";
            this.toStartButton1.Size = new System.Drawing.Size(36, 30);
            this.toStartButton1.SourceForm = null;
            this.toStartButton1.TabIndex = 15;
            this.toStartButton1.Load += new System.EventHandler(this.toStartButton1_Load);
            // 
            // QuestionRandomizer
            // 
            this.QuestionRandomizer.EasyCounter = 0;
            this.QuestionRandomizer.HardCounter = 0;
            this.QuestionRandomizer.IsLimit = false;
            this.QuestionRandomizer.IsRandom = false;
            // 
            // OptionsRandomizer
            // 
            this.OptionsRandomizer.QuestionOptions = null;
            // 
            // toolTip1
            // 
            this.toolTip1.IsBalloon = true;
            this.toolTip1.Popup += new System.Windows.Forms.PopupEventHandler(this.toolTip1_Popup);
            // 
            // StudyForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(807, 658);
            this.Controls.Add(this.toStartButton1);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.ProgressLabel);
            this.Controls.Add(this.QuestionLabel);
            this.Controls.Add(this.pictureBox);
            this.Name = "StudyForm";
            this.Text = "Nauka";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox;
        private System.Windows.Forms.Label ProgressLabel;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button SubmitButton;
        private System.Windows.Forms.RadioButton QuestionOptionRadio3;
        private System.Windows.Forms.RadioButton QuestionOptionRadio2;
        private System.Windows.Forms.RadioButton QuestionOptionRadio1;
        private Components.Components.QuestionRandomizer QuestionRandomizer;
        private Components.Components.OptionsRandomizer OptionsRandomizer;
        private Components.Controls.ToStartButton toStartButton1;
        private System.Windows.Forms.RichTextBox QuestionLabel;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ToolTip toolTip1;
    }
}