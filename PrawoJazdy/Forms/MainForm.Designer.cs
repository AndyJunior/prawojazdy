﻿
using Components.Components;

namespace PrawoJazdy
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.ExamButton = new System.Windows.Forms.Button();
            this.LearnButton = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.bazaDanychToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dodajToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Randomizer = new Components.Components.QuestionRandomizer(this.components);
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Montserrat", 34.25F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(294, 37);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(472, 63);
            this.label1.TabIndex = 0;
            this.label1.Text = "Prawo jazdy kat. B";
            // 
            // ExamButton
            // 
            this.ExamButton.Font = new System.Drawing.Font("Roboto", 16.25F);
            this.ExamButton.Location = new System.Drawing.Point(491, 552);
            this.ExamButton.Name = "ExamButton";
            this.ExamButton.Size = new System.Drawing.Size(221, 88);
            this.ExamButton.TabIndex = 1;
            this.ExamButton.Text = "Egzamin";
            this.ExamButton.UseVisualStyleBackColor = true;
            this.ExamButton.Click += new System.EventHandler(this.StartButton_Click);
            // 
            // LearnButton
            // 
            this.LearnButton.Font = new System.Drawing.Font("Roboto", 16.25F);
            this.LearnButton.Location = new System.Drawing.Point(70, 552);
            this.LearnButton.Name = "LearnButton";
            this.LearnButton.Size = new System.Drawing.Size(214, 88);
            this.LearnButton.TabIndex = 4;
            this.LearnButton.Text = "Nauka";
            this.LearnButton.UseVisualStyleBackColor = true;
            this.LearnButton.Click += new System.EventHandler(this.LearnButton_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1,
            this.bazaDanychToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(766, 27);
            this.menuStrip1.TabIndex = 5;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem2});
            this.toolStripMenuItem1.Font = new System.Drawing.Font("Roboto", 10F);
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(65, 23);
            this.toolStripMenuItem1.Text = "Pomoc";
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(180, 24);
            this.toolStripMenuItem2.Text = "O programie";
            this.toolStripMenuItem2.Click += new System.EventHandler(this.toolStripMenuItem2_Click);
            // 
            // bazaDanychToolStripMenuItem
            // 
            this.bazaDanychToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dodajToolStripMenuItem});
            this.bazaDanychToolStripMenuItem.Font = new System.Drawing.Font("Roboto", 10F);
            this.bazaDanychToolStripMenuItem.Name = "bazaDanychToolStripMenuItem";
            this.bazaDanychToolStripMenuItem.Size = new System.Drawing.Size(102, 23);
            this.bazaDanychToolStripMenuItem.Text = "Baza danych";
            // 
            // dodajToolStripMenuItem
            // 
            this.dodajToolStripMenuItem.Name = "dodajToolStripMenuItem";
            this.dodajToolStripMenuItem.Size = new System.Drawing.Size(147, 24);
            this.dodajToolStripMenuItem.Text = "Zaloguj się";
            this.dodajToolStripMenuItem.Click += new System.EventHandler(this.dodajToolStripMenuItem_Click);
            // 
            // Randomizer
            // 
            this.Randomizer.EasyCounter = 20;
            this.Randomizer.HardCounter = 12;
            this.Randomizer.IsLimit = true;
            this.Randomizer.IsRandom = false;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.BackgroundImage = global::PrawoJazdy.Properties.Resources.ninja_marek;
            this.ClientSize = new System.Drawing.Size(766, 679);
            this.Controls.Add(this.LearnButton);
            this.Controls.Add(this.ExamButton);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MainForm";
            this.Text = "Prawo Jazdy";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button ExamButton;
        private System.Windows.Forms.Button LearnButton;
        private QuestionRandomizer Randomizer;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem bazaDanychToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dodajToolStripMenuItem;
    }
}

