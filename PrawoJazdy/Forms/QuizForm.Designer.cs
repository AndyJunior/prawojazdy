﻿
namespace PrawoJazdy
{
    partial class QuizForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.QuestionLabel = new System.Windows.Forms.RichTextBox();
            this.toStartButton1 = new Components.Controls.ToStartButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.countDownTimer1 = new Components.Controls.CountDownTimer();
            this.SubmitButton = new System.Windows.Forms.Button();
            this.QuestionOptionRadio3 = new System.Windows.Forms.RadioButton();
            this.QuestionOptionRadio2 = new System.Windows.Forms.RadioButton();
            this.QuestionOptionRadio1 = new System.Windows.Forms.RadioButton();
            this.pictureBox = new System.Windows.Forms.PictureBox();
            this.ProgressLabel = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.Control;
            this.panel1.Controls.Add(this.QuestionLabel);
            this.panel1.Controls.Add(this.toStartButton1);
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Controls.Add(this.pictureBox);
            this.panel1.Controls.Add(this.ProgressLabel);
            this.panel1.Location = new System.Drawing.Point(13, 13);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(790, 627);
            this.panel1.TabIndex = 2;
            // 
            // QuestionLabel
            // 
            this.QuestionLabel.BackColor = System.Drawing.SystemColors.Control;
            this.QuestionLabel.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.QuestionLabel.Font = new System.Drawing.Font("Roboto", 14.25F);
            this.QuestionLabel.Location = new System.Drawing.Point(7, 278);
            this.QuestionLabel.Name = "QuestionLabel";
            this.QuestionLabel.ReadOnly = true;
            this.QuestionLabel.Size = new System.Drawing.Size(779, 64);
            this.QuestionLabel.TabIndex = 11;
            this.QuestionLabel.Text = "Pytanie";
            // 
            // toStartButton1
            // 
            this.toStartButton1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.toStartButton1.DestinationForm = this;
            this.toStartButton1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.toStartButton1.ForeColor = System.Drawing.Color.Red;
            this.toStartButton1.Location = new System.Drawing.Point(753, 5);
            this.toStartButton1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.toStartButton1.Name = "toStartButton1";
            this.toStartButton1.Size = new System.Drawing.Size(34, 30);
            this.toStartButton1.SourceForm = null;
            this.toStartButton1.TabIndex = 9;
            this.toStartButton1.Click += new System.EventHandler(this.toStartButton1_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.SystemColors.Control;
            this.groupBox1.Controls.Add(this.countDownTimer1);
            this.groupBox1.Controls.Add(this.SubmitButton);
            this.groupBox1.Controls.Add(this.QuestionOptionRadio3);
            this.groupBox1.Controls.Add(this.QuestionOptionRadio2);
            this.groupBox1.Controls.Add(this.QuestionOptionRadio1);
            this.groupBox1.Font = new System.Drawing.Font("Roboto", 12.25F);
            this.groupBox1.Location = new System.Drawing.Point(7, 348);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(780, 279);
            this.groupBox1.TabIndex = 8;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Odpowiedzi";
            // 
            // countDownTimer1
            // 
            this.countDownTimer1.BackColor = System.Drawing.SystemColors.Control;
            this.countDownTimer1.Font = new System.Drawing.Font("Roboto", 10F);
            this.countDownTimer1.Location = new System.Drawing.Point(703, 12);
            this.countDownTimer1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.countDownTimer1.Name = "countDownTimer1";
            this.countDownTimer1.SetTimer = System.TimeSpan.Parse("00:25:00");
            this.countDownTimer1.Size = new System.Drawing.Size(76, 38);
            this.countDownTimer1.StepMs = 1000;
            this.countDownTimer1.TabIndex = 9;
            // 
            // SubmitButton
            // 
            this.SubmitButton.Location = new System.Drawing.Point(614, 220);
            this.SubmitButton.Name = "SubmitButton";
            this.SubmitButton.Size = new System.Drawing.Size(165, 56);
            this.SubmitButton.TabIndex = 4;
            this.SubmitButton.Text = "Następne pytanie";
            this.SubmitButton.UseVisualStyleBackColor = true;
            this.SubmitButton.Click += new System.EventHandler(this.SubmitButton_Click);
            // 
            // QuestionOptionRadio3
            // 
            this.QuestionOptionRadio3.AutoSize = true;
            this.QuestionOptionRadio3.Location = new System.Drawing.Point(24, 118);
            this.QuestionOptionRadio3.Name = "QuestionOptionRadio3";
            this.QuestionOptionRadio3.Size = new System.Drawing.Size(128, 27);
            this.QuestionOptionRadio3.TabIndex = 2;
            this.QuestionOptionRadio3.TabStop = true;
            this.QuestionOptionRadio3.Text = "radioButton3";
            this.QuestionOptionRadio3.UseVisualStyleBackColor = true;
            // 
            // QuestionOptionRadio2
            // 
            this.QuestionOptionRadio2.AutoSize = true;
            this.QuestionOptionRadio2.Location = new System.Drawing.Point(24, 79);
            this.QuestionOptionRadio2.Name = "QuestionOptionRadio2";
            this.QuestionOptionRadio2.Size = new System.Drawing.Size(128, 27);
            this.QuestionOptionRadio2.TabIndex = 1;
            this.QuestionOptionRadio2.TabStop = true;
            this.QuestionOptionRadio2.Text = "radioButton2";
            this.QuestionOptionRadio2.UseVisualStyleBackColor = true;
            // 
            // QuestionOptionRadio1
            // 
            this.QuestionOptionRadio1.AutoSize = true;
            this.QuestionOptionRadio1.Location = new System.Drawing.Point(24, 39);
            this.QuestionOptionRadio1.Name = "QuestionOptionRadio1";
            this.QuestionOptionRadio1.Size = new System.Drawing.Size(128, 27);
            this.QuestionOptionRadio1.TabIndex = 0;
            this.QuestionOptionRadio1.TabStop = true;
            this.QuestionOptionRadio1.Text = "radioButton1";
            this.QuestionOptionRadio1.UseVisualStyleBackColor = true;
            // 
            // pictureBox
            // 
            this.pictureBox.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox.Location = new System.Drawing.Point(3, -1);
            this.pictureBox.Name = "pictureBox";
            this.pictureBox.Size = new System.Drawing.Size(783, 251);
            this.pictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox.TabIndex = 7;
            this.pictureBox.TabStop = false;
            // 
            // ProgressLabel
            // 
            this.ProgressLabel.AutoSize = true;
            this.ProgressLabel.Font = new System.Drawing.Font("Roboto", 12.25F, System.Drawing.FontStyle.Bold);
            this.ProgressLabel.Location = new System.Drawing.Point(318, 253);
            this.ProgressLabel.Name = "ProgressLabel";
            this.ProgressLabel.Size = new System.Drawing.Size(62, 23);
            this.ProgressLabel.TabIndex = 10;
            this.ProgressLabel.Text = "label1";
            // 
            // QuizForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(815, 651);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "QuizForm";
            this.Text = "Egzamin";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private Components.Components.OptionsRandomizer OptionsRandomizer;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pictureBox;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button SubmitButton;
        private System.Windows.Forms.RadioButton QuestionOptionRadio3;
        private System.Windows.Forms.RadioButton QuestionOptionRadio2;
        private System.Windows.Forms.RadioButton QuestionOptionRadio1;
        private Components.Controls.CountDownTimer countDownTimer1;
        private Components.Controls.ToStartButton toStartButton1;
        private System.Windows.Forms.Label ProgressLabel;
        private System.Windows.Forms.RichTextBox QuestionLabel;
    }
}