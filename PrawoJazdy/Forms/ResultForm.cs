﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using Database.Model;
using Database.Services;

namespace PrawoJazdy
{
    public partial class ResultForm : Form
    {
        private int pointA;
        private int pointB;
        private int result;
        private bool isExam;
        public ResultForm()
        {
            InitializeComponent();
            Init(DatabaseService.GetEasyOrderedAnswers().ToList(),checkedListBox1);
            Init(DatabaseService.GetHardOrderedAnswers().ToList(), checkedListBox2);
            Result();
            SetResult();
        }
        public ResultForm(bool isExam)
        {
            this.isExam = isExam;
            InitializeComponent();
            Init(DatabaseService.GetEasyOrderedAnswers().ToList(), checkedListBox1);
            Init(DatabaseService.GetHardOrderedAnswers().ToList(), checkedListBox2);
            Result();
            if (isExam)
            {
                SetResult();
            }
            else
            {
                this.label2.Visible = false;
                this.label3.Visible = false;
                this.checkedListBox1.Enabled = true;
                this.checkedListBox2.Enabled = true;
            }
        }

        private void SetResult()
        {

            label1.Text = "Wynik: ";
            if (result < 70)
            {
                label1.Text += "nie zdałeś";
                label1.ForeColor = Color.Red;
            }
            else
            {
                label1.Text = "zdałeś";
                label1.ForeColor = Color.Green;
            }
            label2.Text += pointA + " / " + checkedListBox1.Items.Count * 2;
            label3.Text += pointB + " / " + (checkedListBox2.Items.Count * 3);
            
        }

        

        private void Init(List<Answer> answers, CheckedListBox checkedList)
        {
            int counter = 0;
            foreach (var answer in answers)
            {
                checkedList.Items.Add(answer.Question.Text);
                checkedList.SetItemChecked(counter, answer.QuestionOption != null && answer.QuestionOption.IsCorrect);
                counter++;
            }
        }

        private void toStartButton1_Load(object sender, EventArgs e)
        {
            toStartButton1.DestinationForm = new MainForm();
            toStartButton1.SourceForm = this;
        }

        private void Result()
        { 
            pointA = checkedListBox1.CheckedItems.Count * 2;
            pointB = checkedListBox2.CheckedItems.Count * 3;
            result = pointA + pointB;
            
            
        }
    }
}
