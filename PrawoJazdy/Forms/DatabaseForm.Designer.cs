﻿
namespace PrawoJazdy.Forms
{
    partial class DatabaseForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.toStartButton1 = new Components.Controls.ToStartButton();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.textDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.isHardDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.photoPathDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.questionsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.prawoJazdyDataSetBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.prawoJazdyDataSet = new PrawoJazdy.PrawoJazdyDataSet();
            this.questionsTableAdapter = new PrawoJazdy.PrawoJazdyDataSetTableAdapters.QuestionsTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.questionsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.prawoJazdyDataSetBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.prawoJazdyDataSet)).BeginInit();
            this.SuspendLayout();
            // 
            // toStartButton1
            // 
            this.toStartButton1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.toStartButton1.DestinationForm = null;
            this.toStartButton1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.toStartButton1.ForeColor = System.Drawing.Color.Red;
            this.toStartButton1.Location = new System.Drawing.Point(870, 14);
            this.toStartButton1.Name = "toStartButton1";
            this.toStartButton1.Size = new System.Drawing.Size(42, 33);
            this.toStartButton1.SourceForm = null;
            this.toStartButton1.TabIndex = 1;
            this.toStartButton1.Load += new System.EventHandler(this.toStartButton1_Load);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.textDataGridViewTextBoxColumn,
            this.isHardDataGridViewCheckBoxColumn,
            this.photoPathDataGridViewTextBoxColumn});
            this.dataGridView1.DataSource = this.questionsBindingSource;
            this.dataGridView1.Location = new System.Drawing.Point(47, 14);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(807, 435);
            this.dataGridView1.TabIndex = 2;
            // 
            // textDataGridViewTextBoxColumn
            // 
            this.textDataGridViewTextBoxColumn.DataPropertyName = "Text";
            this.textDataGridViewTextBoxColumn.HeaderText = "Text";
            this.textDataGridViewTextBoxColumn.Name = "textDataGridViewTextBoxColumn";
            this.textDataGridViewTextBoxColumn.Width = 530;
            // 
            // isHardDataGridViewCheckBoxColumn
            // 
            this.isHardDataGridViewCheckBoxColumn.DataPropertyName = "IsHard";
            this.isHardDataGridViewCheckBoxColumn.HeaderText = "IsHard";
            this.isHardDataGridViewCheckBoxColumn.Name = "isHardDataGridViewCheckBoxColumn";
            // 
            // photoPathDataGridViewTextBoxColumn
            // 
            this.photoPathDataGridViewTextBoxColumn.DataPropertyName = "PhotoPath";
            this.photoPathDataGridViewTextBoxColumn.HeaderText = "PhotoPath";
            this.photoPathDataGridViewTextBoxColumn.Name = "photoPathDataGridViewTextBoxColumn";
            // 
            // questionsBindingSource
            // 
            this.questionsBindingSource.DataMember = "Questions";
            this.questionsBindingSource.DataSource = this.prawoJazdyDataSetBindingSource;
            // 
            // prawoJazdyDataSetBindingSource
            // 
            this.prawoJazdyDataSetBindingSource.DataSource = this.prawoJazdyDataSet;
            this.prawoJazdyDataSetBindingSource.Position = 0;
            // 
            // prawoJazdyDataSet
            // 
            this.prawoJazdyDataSet.DataSetName = "PrawoJazdyDataSet";
            this.prawoJazdyDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // questionsTableAdapter
            // 
            this.questionsTableAdapter.ClearBeforeFill = true;
            // 
            // DatabaseForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(924, 503);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.toStartButton1);
            this.Name = "DatabaseForm";
            this.Text = "Baza danych";
            this.Load += new System.EventHandler(this.DatabaseForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.questionsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.prawoJazdyDataSetBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.prawoJazdyDataSet)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private Components.Controls.ToStartButton toStartButton1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.BindingSource prawoJazdyDataSetBindingSource;
        private PrawoJazdyDataSet prawoJazdyDataSet;
        private System.Windows.Forms.BindingSource questionsBindingSource;
        private PrawoJazdyDataSetTableAdapters.QuestionsTableAdapter questionsTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn textDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn isHardDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn photoPathDataGridViewTextBoxColumn;
    }
}