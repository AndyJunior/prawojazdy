﻿namespace Database.Model
{
    public class Answer
    {
        public int Id { get; set; }

        public Question Question { get; set; }

        public QuestionOption QuestionOption { get; set; }
    }
}
