﻿namespace Database.Model
{
    public class Question
    {
        public int Id { get; set; }

        public string Text { get; set; }

        public bool IsHard { get; set; }

        public string PhotoPath { get; set; }

        public string CorrectDescription { get; set; }

        public int Points { get; set; }

    }
}
