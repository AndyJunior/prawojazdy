﻿using System.Collections.Generic;
using System.Linq;
using Database.Config;
using Database.Model;

namespace Database.Services
{
    public static class DatabaseService
    {
        private static readonly AppQuizContext _context = new AppQuizContext();

        public static IEnumerable<Question> GetAllQuestions()
        {
            return _context.Questions;
        }

        public static IEnumerable<Question> GetEasyQuestions()
        {
            return _context.Questions.Where(question => !question.IsHard);
        }

        public static int GetEasyQuestionsCount()
        {
            return _context.Questions.Count(question => !question.IsHard);
        }
        public static int GetHardQuestionsCount()
        {
            return _context.Questions.Count(question => question.IsHard);
        }
        public static IEnumerable<Question> GetHardQuestions()
        {
            return _context.Questions.Where(question => question.IsHard);
        }
        public static IEnumerable<QuestionOption> GetQuestionOptions(int id)
        {
            return _context.QuestionOptions.Where(option => option.Question.Id == id);
        }

        public static void CreateAnswer(Question question, QuestionOption questionOption)
        {

            _context.Answers.Add(new Answer()
            {
                Question = question,
                QuestionOption = questionOption
            });
            _context.SaveChanges();
        }

        public static IEnumerable<Answer> GetAllOrderedAnswers()
        {
            return _context.Answers.OrderBy(a => a.Question.Id);
        }
        public static IEnumerable<Answer> GetEasyOrderedAnswers()
        {
            return _context.Answers.Where(answer => !answer.Question.IsHard).OrderBy(a => a.Question.Id);
        }
        public static IEnumerable<Answer> GetHardOrderedAnswers()
        {
            return _context.Answers.Where(answer => answer.Question.IsHard).OrderBy(a => a.Question.Id);
        }
        public static void RemoveAnswers()
        {
            foreach (var answer in _context.Answers)
            {
                _context.Answers.Remove(answer);
            }
        }

        public static QuestionOption GetCorrectOption(int questionId)
        {
            return _context.QuestionOptions.FirstOrDefault(q => q.IsCorrect && q.Question.Id == questionId);
        }
        public static string GetCorrectDescription(int questionId)
        {
            var description = _context.Questions.FirstOrDefault(q => q.Id == questionId).CorrectDescription;
            if (description == null)
            {
                return null;
            }

            return description;
        }
    }
}
