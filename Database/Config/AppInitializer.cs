﻿using Database.Model;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Database.Config
{
    public class AppInitializer : DropCreateDatabaseAlways<AppQuizContext>
    {
        protected override void Seed(AppQuizContext context)
        {
            IList<Question> questions = new List<Question>()
            {
                new Question()
                {
                    PhotoPath = "[0]Pytanie_zawracanie.png",
                    Text = "Czy w widocznej sytuacji dopuszczalne jest zawracanie?",
                    IsHard = false,
                    CorrectDescription = "Na pasie dzielącym jezdnie umieszczono zakaz zawracania.",
                    Points = 2
                },

                new Question()
                {
                    PhotoPath = "[1]Pytanie_ostrzegawcze.png",
                    Text = "Czy umieszczona pod znakiem ostrzegawczym widocznym na zdjęciu tabliczka," +
                           "wskazuje jego odległość od miejsca niebezpiecznego?",
                    IsHard = false,
                    CorrectDescription =
                        "Tabliczka umieszczona pod znakiem wskazuje odległość znaku ostrzegawczego od " +
                        "niebezpiecznego miejsca.",
                    Points = 2
                },

                new Question()
                {
                    PhotoPath = "[2]Pytanie_podporzadkowana.png",
                    Text = "Czy w tej sytuacji, na najbliższym skrzyżowaniu," +
                           "masz pierwszeństwo przed pojazdami nadjeżdżającymi z lewej strony?",
                    IsHard = false,
                    CorrectDescription = "Pojazdy nadjeżdżające z lewej strony poruszają się drogą podporządkowaną.",
                    Points = 2
                },

                new Question()
                {
                    PhotoPath = "[3]Pytanie_wlot.png",
                    Text = "Czy w przedstawionej sytuacji jesteś " + "ostrzegany o wlocie drogi z prawej strony?",
                    IsHard = false,
                    CorrectDescription =
                        "Znak widoczny na zdjęciu ostrzega o występującym po prawej stronie jezdni zwężeniu, " +
                        "które może powodować utrudnienia ruchu.",
                    Points = 2
                },

                new Question()
                {
                    PhotoPath = "[4]Pytanie_wzniesienie.png",
                    Text = "Czy w przedstawionej sytuacji jesteś " + "ostrzegany o znacznym wzniesieniu drogi?",
                    IsHard = false,
                    CorrectDescription = "Znak widoczny na zdjęciu ostrzega o znacznym wzniesieniu drogi.",
                    Points = 2
                },

                new Question()
                {
                    PhotoPath = "[5]Pytanie_przebieg.png",
                    Text = "Czy tabliczka umieszczona pod znakiem wskazuje rzeczywisty " +
                           "przebieg drogi z pierwszeństwem przejazdu?",
                    IsHard = false,
                    CorrectDescription =
                        "Tabliczka umieszczona pod znakiem A-7 (Ustąp pierwszeństwa) wskazuje rzeczywisty przebieg drogi " +
                        "z pierwszeństwem przez skrzyżowanie.",
                    Points = 2
                },

                new Question()
                {
                    PhotoPath = "[6]Pytanie_niebezp.png",
                    Text = "Czy w przedstawionej sytuacji odległość znaku ostrzegawczego" +
                           " od miejsca niebezpiecznego wynosi od 150 do 300 metrów?",
                    IsHard = false,
                    CorrectDescription =
                        "Tabliczka umieszczona pod znakiem wskazuje odległość znaku ostrzegawczego od niebezpiecznego miejsca.",
                    Points = 2
                },

                new Question()
                {
                    PhotoPath = "[7]Pytanie_zimno.png", Text = "Czy ten znak ostrzega o możliwości poślizgu " +
                                                               "pojazdu spowodowanego zawilgoceniem jezdni?",
                    IsHard = false,
                    CorrectDescription =
                        "Znak widoczny na zdjęciu ostrzega o mogącym występować na drodze oszronieniu jezdni lub gołoledzi.",
                    Points = 2
                },

                new Question()
                {
                    PhotoPath = "[8]Pytanie_ruch.png", Text = "Czy w przedstawionej sytuacji jesteś ostrzegany o" +
                                                              " zbliżaniu się do miejsca, w którym rozpoczyna się ruch dwukierunkowy?",
                    IsHard = false,
                    CorrectDescription =
                        "Znak widoczny na zdjęciu ostrzega jadących jezdnią jednokierunkową o miejscu, w którym rozpoczyna się ruch dwukierunkowy.",
                    Points = 2
                },

                new Question()
                {
                    PhotoPath = "[9]Pytanie_zakret.png", Text = "Czy w przedstawionej sytuacji jesteś ostrzegany o " +
                                                                "dwóch niebezpiecznych zakrętach, z których pierwszy jest w lewo?",
                    IsHard = false,
                    CorrectDescription =
                        "Znak widoczny na zdjęciu ostrzega o dwóch niebezpiecznych zakrętach, z których pierwszy jest w kierunku wskazanym na znaku, a drugi może być zarówno w lewo, jak i w prawo.",
                    Points = 2
                },

                new Question()
                {
                    PhotoPath = "[10]Pytanie_opady.png", Text = "Czy w przedstawionej sytuacji" +
                                                                " jesteś ostrzegany o opadach śniegu?",
                    IsHard = false,
                    CorrectDescription =
                        "Znak widoczny na zdjęciu ostrzega o mogącym występować na drodze oszronieniu jezdni lub gołoledzi.",
                    Points = 2
                },

                new Question()
                {
                    PhotoPath = "[11]Pytanie_ciuchciuch.png", Text =
                        "Czy w przedstawionej sytuacji jesteś ostrzegany o zbliżaniu " +
                        "się do przejazdu kolejowego, wyposażonego w zapory lub półzapory?",
                    IsHard = false,
                    CorrectDescription =
                        "Znak widoczny na zdjęciu ostrzega o przejeździe kolejowym wyposażonym w zapory lub półzapory.",
                    Points = 2
                },

                new Question()
                {
                    PhotoPath = "[12]Pytanie_koleiny.png", Text = "Czy widoczny znak ostrzega o odcinku drogi," +
                                                                  " na którym należy zachować szczególną ostrożność ze względu na koleiny w nawierzchni?",
                    IsHard = false,
                    CorrectDescription =
                        "W każdej sytuacji poprzedzonej znakiem ostrzegawczym należy zachować szczególną ostrożność.",
                    Points = 2
                },

                new Question()
                {
                    PhotoPath = "[13]Pytanie_skoczki.png", Text = "Czy w przedstawionej sytuacji" +
                                                                  " jesteś ostrzegany o poprzecznej nierówności jezdni?",
                    IsHard = false,
                    CorrectDescription = "Znak widoczny na zdjęciu ostrzega o poprzecznej nierówności jezdni.",
                    Points = 2
                },

                new Question()
                {
                    PhotoPath = "[14]Pytanie_hau.png", Text = "Czy po zatrzymaniu pojazdu do kontroli," +
                                                              " kierujący pojazdem powinien trzymać ręce na kierownicy?",
                    IsHard = false,
                    CorrectDescription =
                        "Po zatrzymaniu pojazdu do kontroli, kierujący pojazdem powinien trzymać ręce na kierownicy.",
                    Points = 2
                },

                new Question()
                {
                    PhotoPath = "[15]Pytanie_pierwszenstwo.png", Text = "Czy w tej sytuacji zbliżasz się " +
                                                                        "do drogi z pierwszeństwem?",
                    IsHard = false,
                    CorrectDescription = "Znak A-7 świadczy o zbliżaniu się do skrzyżowania z drogą z pierwszeństwem.",
                    Points = 2
                },

                new Question()
                {
                    PhotoPath = "[16]Pytanie_ustap.png", Text = "Czy masz obowiązek każdorazowo zatrzymać" +
                                                                " pojazd przed wjazdem na takie skrzyżowanie?",
                    IsHard = false, CorrectDescription = "Obowiązek zatrzymania nakłada znak STOP ", 
                    Points = 2
                },

                new Question()
                {
                    PhotoPath = "[17]Pytanie_hopka.png", Text = "Czy w przedstawionej sytuacji jesteś ostrzegany " +
                                                                "o wypukłości na jezdni zastosowanej w celu spowolnienia ruchu?",
                    IsHard = false,
                    CorrectDescription =
                        "Znak widoczny na zdjęciu ostrzega o wypukłości na jezdni zastosowanej w celu spowolnienia ruchu.",
                    Points = 2
                },

                new Question()
                {
                    PhotoPath = "[18]Pytanie_wylot.png", Text = "Czy w przedstawionej sytuacji jesteś ostrzegany o " +
                                                                "zbliżaniu się do skrzyżowania z jednokierunkową drogą podporządkowaną?",
                    IsHard = false,
                    CorrectDescription =
                        "Znak widoczny na zdjęciu ostrzega o skrzyżowaniu z jednokierunkową drogą podporządkowaną, której wlot występuje po stronie lewej.",
                    Points = 2
                },

                new Question()
                {
                    PhotoPath = "[19]Pytanie_zakret.png", Text = "Czy w przedstawionej sytuacji jesteś ostrzegany o" +
                                                                 " dwóch niebezpiecznych zakrętach?",
                    IsHard = false,
                    CorrectDescription =
                        "Widoczna na zdjęciu tabliczka umieszczona pod znakiem informuje o ilości niebezpiecznych zakrętów.",
                    Points = 2
                },

                new Question()
                {
                    PhotoPath = "[20]Pytanie_rownorzedne.png", Text = "Czy na tak oznakowanym skrzyżowaniu masz " +
                                                                      "pierwszeństwo przed wszystkimi pojazdami?",
                    IsHard = false,
                    CorrectDescription =
                        "Na skrzyżowaniu równorzędnym należy ustąpić pierwszeństwa pojazdowi nadjeżdżającemu z prawej strony.",
                    Points = 2
                },

                new Question()
                {
                    PhotoPath = "[21]Pytanie_zwezenie.png", Text =
                        "Czy w tej sytuacji wolno Ci wjechać na zwężony odcinek jezdni, " +
                        "jeżeli nie wymusza to na nadjeżdżającym z przeciwka konieczności zatrzymania się?",
                    IsHard = false,
                    CorrectDescription =
                        "Znak zabrania kierującym wjazdu lub wejścia na zwężony odcinek jezdni, jeżeli zmusiłoby to kierujących znajdujących się na tym odcinku lub zbliżających się do niego z przeciwnej strony do zatrzymania się lub zmiany toru jazdy.",
                    Points = 2
                },

                new Question()
                {
                    PhotoPath = "[22]Pytanie_skret.png", Text = "Czy w tej sytuacji z pasa ruchu, " +
                                                                "który zajmujesz, dozwolone jest wykonanie skrętu w prawo?",
                    IsHard = false,
                    CorrectDescription =
                        "W związku z umieszczonym znakiem nakazu cała szerokość jezdni służy do skrętu w prawo.",
                    Points = 2
                },

                new Question()
                {
                    PhotoPath = "[23]Pytanie_zatrzymanie.png", Text = "Czy w przedstawionej sytuacji" +
                                                                      " możesz zatrzymać pojazd przed znakiem na poboczu?",
                    IsHard = false,
                    CorrectDescription =
                        "Zakaz zatrzymywania obowiązuje do miejsca jego ustawienia (tabliczka pod znakiem).",
                    Points = 2
                },

                new Question()
                {
                    PhotoPath = "[24]Pytanie_oplaty.png", Text =
                        "Czy umieszczona pod widocznym znakiem zakazu tabliczka" +
                        " wskazuje odległość od miejsca, w którym ten zakaz obowiązuje?",
                    IsHard = false,
                    CorrectDescription =
                        "Umieszczona pod widocznym znakiem zakazu tabliczka wskazuje odległość od miejsca, w którym ten zakaz obowiązuje.",
                    Points = 2
                },

                new Question()
                {
                    PhotoPath = "[25]Pytanie_skret.png", Text = "Czy w tej sytuacji wolno Ci " +
                                                                "skręcić w lewo na najbliższym skrzyżowaniu?",
                    IsHard = false, CorrectDescription = "Zakaz zawracania umożliwia wykonanie skrętu w lewo.",
                    Points = 2
                },

                new Question()
                {
                    PhotoPath = "[26]Pytanie_swiatla.png", Text = "Czy w widocznej sytuacji, w czasie nadawania " +
                                                                  "tego sygnału świetlnego masz prawo skręcić w lewo na skrzyżowaniu?",
                    IsHard = false, CorrectDescription = "Nad sygnalizatorem umieszczono zakaz skrętu w lewo.",
                    Points = 2
                },

                new Question()
                {
                    PhotoPath = "[27]Pytanie_zakaz.png", Text = "Czy w tej sytuacji zakaz wyrażony" +
                                                                " znakiem obowiązuje od miejsca jego ustawienia?",
                    IsHard = false,
                    CorrectDescription =
                        "Pod znakiem umieszczono tabliczkę wskazującą odległość znaku od miejsca w którym znak obowiązuje.",
                    Points = 2
                },

                new Question()
                {
                    PhotoPath = "[28]Pytanie_chlup.png", Text = "Czy w tej sytuacji, kierując " +
                                                                "samochodem osobowym, możesz wjechać na most?",
                    IsHard = false,
                    CorrectDescription =
                        "Znak widoczny na zdjęciu to zakaz wjazdu pojazdów silnikowych z wyjątkiem motocykli jednośladowych.",
                    Points = 2
                },

                new Question()
                {
                    PhotoPath = "[29]Pytanie_zakazik.png", Text = "Czy znak ustawiony na prawej " +
                                                                  "jezdni oznacza zakaz ruchu w obu kierunkach?",
                    IsHard = false, CorrectDescription = "B-1 - Zakaz ruchu w obu kierunkach.", 
                    Points = 2
                },

                new Question()
                {
                    PhotoPath = "[30]Pytanie_lewus.png",
                    Text = "Czy w przedstawionej sytuacji wolno Ci skręcić w lewo?", IsHard = false,
                    CorrectDescription = "Znak widoczny na zdjęciu oznacza zakaz skręcania w lewo.", 
                    Points = 2
                },

                new Question()
                {
                    PhotoPath = "[31]Pytanie_znaczek.png", Text = "Czy w przedstawionej sytuacji możesz" +
                                                                  " zatrzymać pojazd za znakiem na poboczu?",
                    IsHard = false,
                    CorrectDescription =
                        "Zakaz zatrzymywania obowiązuje do miejsca jego ustawienia (tabliczka pod znakiem).",
                    Points = 2
                },

                new Question()
                {
                    PhotoPath = "[32]Pytanie_wyprzedzanie.png", Text =
                        "Kierujesz pojazdem silnikowym. Czy ten znak zabrania Ci" +
                        " wyprzedzania pojazdów silnikowych wielośladowych?",
                    IsHard = false,
                    CorrectDescription =
                        "Znak widoczny na zdjęciu uniemożliwia wyprzedzanie pojazdów silnikowych, które zostawiają więcej niż jeden ślad.",
                    Points = 2
                },

                new Question()
                {
                    PhotoPath = "[33]Pytanie_jezdnia.png",
                    Text = "Czy w widocznej sytuacji możesz wjechać na zwężony odcinek jezdni?", 
                    IsHard = false,
                    CorrectDescription = "Nic nie nadjeżdża z przeciwka.", 
                    Points = 2
                },

                new Question()
                {
                    PhotoPath = "[34]Pytanie_zamkniete.png", Text = "Czy znak zakazu umieszczony na prawej" +
                                                                    " jezdni oznacza, że jest ona zamknięta dla ruchu?",
                    IsHard = false, CorrectDescription = "Ten znak to zakaz ruchu w obu kierunkach.", 
                    Points = 2
                },

                new Question()
                {
                    PhotoPath = "[35]Pytanie_zatrzymanie.png", Text = "Czy na tym skrzyżowaniu dopuszczalne jest " +
                                                                      "skręcenie w prawo bez uprzedniego zatrzymania się?",
                    IsHard = false, CorrectDescription = "Znak STOP nakłada obowiązek zatrzymania się.", 
                    Points = 2
                },

                new Question()
                {
                    PhotoPath = "[36]Pytanie_zawrotka.png",
                    Text = "Czy w przedstawionej sytuacji dozwolone jest zawracanie?", 
                    IsHard = false,
                    CorrectDescription = "Widoczny znak to zakaz zawracania.", 
                    Points = 2
                },

                new Question()
                {
                    PhotoPath = "[37]Pytanie_stop.png", Text = "Czy w widocznej sytuacji masz prawo przejechać" +
                                                               " przez przejazd bez zatrzymania się, jeżeli nie nadjeżdża pojazd szynowy?",
                    IsHard = false, CorrectDescription = "Znak STOP nakłada obowiązek zatrzymania się.",
                    Points = 2
                },

                new Question()
                {
                    PhotoPath = "[38]Pytanie_zatrzymanie.png", Text = "Czy na tym skrzyżowaniu zatrzymanie " +
                                                                      "pojazdu powinno nastąpić przed znakiem „STOP”?",
                    IsHard = false,
                    CorrectDescription =
                        "Zatrzymanie powinno nastąpić przed linią zatrzymania lub w miejscu o najlepszej widoczności.",
                    Points = 2
                },

                new Question()
                {
                    PhotoPath = "[39]Pytanie_jezdnia.png", Text = "Czy umieszczona pod znakiem zakazu " +
                                                                  "tabliczka wskazuje długość odcinka jezdni, na którym zakaz obowiązuje?",
                    IsHard = false,
                    CorrectDescription =
                        "Umieszczona pod znakiem zakazu tabliczka wskazuje długość odcinka jezdni, na którym zakaz obowiązuje.",
                    Points = 2
                },

                new Question()
                {
                    PhotoPath = "[40]Pytanie_zakaz.png", Text = "Czy widoczny znak zakazu obowiązuje tylko" +
                                                                " do najbliższego skrzyżowania włącznie?",
                    IsHard = false,
                    CorrectDescription =
                        "Zakaz zawracania obowiązuje od miejsca ustawienia znaku do najbliższego skrzyżowania włącznie z tym skrzyżowaniem.",
                    Points = 2
                },

                new Question()
                {
                    PhotoPath = "[41]S_Pytanie_predkosc.png", Text = "Z jaką maksymalną dopuszczalną prędkością " +
                                                                     "wolno Ci kierować samochodem osobowym po drodze za tym znakiem?",
                    IsHard = true,
                    CorrectDescription = "Dopuszczalna prędkość samochodu osobowego na autostradzie wynosi 140 km/h.",
                    Points = 3
                },

                new Question()
                {
                    PhotoPath = "[42]S_Pytanie_minimum.png",
                    Text = "Z jaką prędkością masz obowiązek jechać na autostradzie?", IsHard = true,
                    CorrectDescription =
                        "Na kierującym spoczywa obowiązek poruszania się z prędkością nie utrudniającą jazdy innym kierującym.",
                    Points = 3
                },

                new Question()
                {
                    PhotoPath = "[43]S_Pytanie_eskpress.png", Text =
                        "Z jaką maksymalną dopuszczalną prędkością możesz jechać kierując" +
                        " samochodem osobowym na drodze ekspresowej jednojezdniowej?",
                    IsHard = true,
                    CorrectDescription =
                        "Dopuszczalna prędkość samochodu osobowego na drodze ekspresowej jednojezdniowej wynosi 100 km/h",
                    Points = 3
                },

                new Question()
                {
                    PhotoPath = "[44]S_Pytanie_zabudowany.png", Text =
                        "Z jaką maksymalną dopuszczalną prędkością wolno Ci kierować" +
                        " samochodem osobowym po drodze za tym znakiem w godzinach 23.00-5.00?",
                    IsHard = true,
                    CorrectDescription =
                        "Prędkość dopuszczalna pojazdu lub zespołu pojazdów na obszarze zabudowanym w godzinach 23:00–5:00 wynosi 60 km/h.",
                    Points = 3
                },

                new Question()
                {
                    PhotoPath = "[45]S_Pytanie_krajowka.png", Text = "Z jaką maksymalną dopuszczalną prędkością wolno" +
                                                                     " Ci kierować samochodem osobowym po drodze za tym znakiem?",
                    IsHard = true,
                    CorrectDescription =
                        "Dopuszczalna prędkość samochodu osobowego na drodze jednojezdniowej poza obszarem zabudowanym wynosi 90 km/h.",
                    Points = 3
                },

                new Question()
                {
                    PhotoPath = "[46]S_Pytanie_ograniczenie.png", Text =
                        "Z jaką maksymalną dopuszczalną prędkością wolno Ci " +
                        "kierować samochodem osobowym po drodze za tym znakiem zakazu?",
                    IsHard = true,
                    CorrectDescription =
                        "Znak ograniczenie prędkości oznacza zakaz przekraczania prędkości określonej na znaku liczbą kilometrów na godzinę.",
                    Points = 3
                },

                new Question()
                {
                    PhotoPath = "[47]S_Pytanie_poza.png", Text = "Z jaką maksymalną dopuszczalną prędkością możesz " +
                                                                 "jechać samochodem osobowym na drodze za tym znakiem?",
                    IsHard = true,
                    CorrectDescription =
                        "Dopuszczalna prędkość samochodu osobowego na drodze jednojezdniowej poza obszarem zabudowanym wynosi 90 km/h.",
                    Points = 3
                },

                new Question()
                {
                    PhotoPath = "[48]S_Pytanie_przyczepka.png", Text = "Z jaką prędkością masz prawo poruszać się" +
                                                                       " kierując samochodem osobowym z przyczepą lekką po autostradzie?",
                    IsHard = true,
                    CorrectDescription = "Samochodem osobowym z przyczepą nie można przekroczyć 80 km/h.",
                    Points = 3
                },

                new Question()
                {
                    PhotoPath = "[49]S_Pytanie_dwujezd.png", Text =
                        "Kierujesz samochodem osobowym. Z jaką maksymalnie dopuszczalną " +
                        "prędkością, masz prawo poruszać się na tej drodze za tym znakiem?",
                    IsHard = true, CorrectDescription = "Na drodze dwujezdniowej poza obszarem zabudowanym 100 km/h.",
                    Points = 3
                },

                new Question()
                {
                    PhotoPath = "[50]S_Pytanie_holowanie.png", Text =
                        "Jakiej prędkości nie możesz przekraczać na obszarze " +
                        "zabudowanym, holując samochodem osobowym inny pojazd silnikowy?",
                    IsHard = true,
                    CorrectDescription =
                        "Kierujący może holować pojazd silnikowy tylko pod warunkiem, że: prędkość pojazdu holującego nie przekracza 30 km/h na obszarze zabudowanym i 60 km/h poza tym obszarem.",
                    Points = 3
                },

                new Question()
                {
                    PhotoPath = "[51]S_Pytanie_pasy.png", Text =
                        "Czy podczas jazdy w takich warunkach drogowych powinieneś" +
                        " korzystać z pasów bezpieczeństwa?",
                    IsHard = true,
                    CorrectDescription =
                        "Kierujący pojazdem samochodowym oraz osoba przewożona takim pojazdem wyposażonym w pasy bezpieczeństwa są obowiązani korzystać z tych pasów podczas jazdy,",
                    Points = 3
                },

                new Question()
                {
                    PhotoPath = "[52]S_Pytanie_pasyDla.png", Text =
                        "Jaki wpływ ma stosowanie pasów bezpieczeństwa na powstanie " +
                        "obrażeń ciężkich przy zderzeniach czołowych?",
                    IsHard = true,
                    CorrectDescription =
                        "Korzystanie z pasów zmniejsza ryzyko powstania obrażeń ciężkich lub śmiertelnych.",
                    Points = 3
                },

                new Question()
                {
                    PhotoPath = "[53]S_Pytanie_pasyy.png", Text =
                        "Czy dorosły pasażer samochodu osobowego przewożony na tylnym " +
                        "siedzeniu ma obowiązek używania pasów bezpieczeństwa?",
                    IsHard = true,
                    CorrectDescription =
                        "Kierujący pojazdem samochodowym oraz osoba przewożona takim pojazdem wyposażonym w pasy bezpieczeństwa są obowiązani korzystać z tych pasów podczas jazdy,",
                    Points = 3
                },

                new Question()
                {
                    PhotoPath = "[54]S_Pytanie_trojkat.png", Text =
                        "Jakie jest przeznaczenie trójkąta, który stanowi " +
                        "obowiązkowe wyposażenie samochodu osobowego?",
                    IsHard = true,
                    CorrectDescription =
                        "Trójkąt służy do ostrzegania o unieruchomieniu pojazdu na drodze z powodu awarii.",
                    Points = 3
                },

                new Question()
                {
                    PhotoPath = "[55]S_Pytanie_paski.png", Text =
                        "Które z wymienionych uszkodzeń pasów bezpieczeństwa oznaczają" +
                        " bezwzględną konieczność wymiany pasów na nowe?",
                    IsHard = true,
                    CorrectDescription = "Przetarcia, naderwania pasów świadczą o ich osłabionej wytrzymałości.",
                    Points = 3
                },

                new Question()
                {
                    PhotoPath = "[56]S_Pytanie_pasyCionsza.png",
                    Text =
                        "Która z osób jest zwolniona z obowiązku korzystania z pasów bezpieczeństwa w czasie jazdy samochodem osobowym?",
                    IsHard = true,
                    CorrectDescription =
                        "Obowiązek korzystania z pasów bezpieczeństwa nie dotyczy między innymi kobiety w widocznej ",
                    Points = 3
                },

                new Question()
                {
                    PhotoPath = "[57]S_Pytanie_zaglowki.png", Text = "Jak powinien być ustawiony zagłówek?",
                    IsHard = true, CorrectDescription = "Zagłówek powinien być ustawiony na linii czubka głowy",
                    Points = 3
                },

                new Question()
                {
                    PhotoPath = "[58]S_Pytanie_zaglowekElo.png",
                    Text = "Na którym zdjęciu zagłówek jest dobrze ustawiony?", IsHard = true,
                    CorrectDescription =
                        "Prawidłowo ustawiony zagłówek powinien znajdować się maksymalnie blisko potylicy, a jego górna krawędź powinna znajdować się na wysokości czubka głowy.",
                    Points = 3
                },

                new Question()
                {
                    PhotoPath = "[59]Pytanie_tramwraj.png",
                    Text = "Czy w tej sytuacji musisz przewidywać, że zza tramwaju wyjedzie rowerzysta?",
                    IsHard = false,
                    CorrectDescription =
                        "Kierujący pojazdem zbliżający się do miejsca oznaczonego znakiem jest obowiązany zmniejszyć prędkość tak, aby nie narazić na niebezpieczeństwo pieszych znajdujących się w tych miejscach lub na nie wchodzących oraz rowerzystów znajdujących się w tych miejscach lub na nie wjeżdżających",
                    Points = 2
                },

                new Question()
                {
                    PhotoPath = "[60]Pytanie_rowerzystaa.png",
                    Text = "Czy w przedstawionej sytuacji ustąpisz pierwszeństwa rowerzyście??", IsHard = false,
                    CorrectDescription = "Należy ustąpić pierwszeństwa rowerzyście znajdującemu się na przejeździe.",
                    Points = 2
                },

                new Question()
                {
                    PhotoPath = "[61]Pytanie_dziecioki.png",
                    Text = "Czy w tej sytuacji powinieneś oddalić się od prawej krawędzi jezdni?", IsHard = false,
                    CorrectDescription = "W tej sytuacji należy kierować się zasadą ograniczonego zaufania.", Points = 2
                },

                new Question()
                {
                    PhotoPath = "[62]Pytanie_swiatelka.png", Text =
                        "Czy stojąc przez kilka minut bez ruchu w zatorze drogowym ('korku')," +
                        " pomiędzy innymi pojazdami na tym samym pasie ruchu, masz prawo wyłączyć światła?",
                    IsHard = false,
                    CorrectDescription =
                        "Jeżeli zatrzymanie trwa ponad 1 minutę, dopuszcza się wyłączenie świateł zewnętrznych pojazdu, o ile na tym samym pasie ruchu, przed tym pojazdem i za nim, stoją inne pojazdy.",
                    Points = 2
                },

                new Question()
                {
                    PhotoPath = "[63]Pytanie_titanie.png",
                    Text =
                        "Czy masz prawo użyć sygnału dźwiękowego, by ostrzec innych przed bezpośrednim niebezpieczeństwem?",
                    IsHard = false,
                    CorrectDescription =
                        "Kierujący pojazdem może używać sygnału dźwiękowego lub świetlnego, w razie gdy zachodzi konieczność ostrzeżenia o niebezpieczeństwie.",
                    Points = 2
                },

                new Question()
                {
                    PhotoPath = "[64]Pytanie_swiatela.png", Text =
                        "Czy w tej sytuacji masz prawo wyłączyć światła zewnętrzne pojazdu, jeśli zatrzymałeś pojazd na dłużej niż 1 minutę, " +
                        "a przed i za Tobą stoją inne pojazdy?",
                    IsHard = false,
                    CorrectDescription =
                        "Jeżeli zatrzymanie trwa ponad 1 minutę, dopuszcza się wyłączenie świateł zewnętrznych pojazdu, o ile na tym samym pasie ruchu, przed tym pojazdem i za nim, stoją inne pojazdy.",
                    Points = 2
                },

                new Question()
                {
                    PhotoPath = "[65]Pytanie_swiatlacdn.png",
                    Text =
                        "Czy w tej sytuacji, parkując na drodze, masz obowiązek włączyć światła pozycyjne lub postojowe?",
                    IsHard = false,
                    CorrectDescription =
                        "Kierujący pojazdem silnikowym lub szynowym, w warunkach niedostatecznej widoczności, podczas zatrzymania niewynikającego z warunków ruchu lub przepisów ruchu drogowego oraz podczas postoju, jest obowiązany używać świateł pozycyjnych przednich i tylnych lub świateł postojowych.",
                    Points = 2
                },

                new Question()
                {
                    PhotoPath = "[66]Pytanie_przeciwmgielne.png", Text =
                        "Czy jadąc z włączonymi światłami mijania, w intensywnym deszczu ograniczającym widoczność, " +
                        "masz prawo używać świateł przeciwmgłowych przednich?",
                    IsHard = false,
                    CorrectDescription =
                        "W czasie jazdy w warunkach zmniejszonej przejrzystości powietrza, spowodowanej mgłą, opadami atmosferycznymi lub innymi przyczynami kierujący pojazdem silnikowym jest obowiązany włączyć światła mijania lub przeciwmgłowe przednie albo oba te światła jednocześnie.",
                    Points = 2
                },

                new Question()
                {
                    PhotoPath = "[67]Pytanie_przeciwcdn.png", Text =
                        "Czy masz prawo używać świateł przeciwmgłowych tylnych, " +
                        "jeżeli mgła ogranicza widoczność na odległość mniejszą niż 50 m?",
                    IsHard = false,
                    CorrectDescription =
                        "Kierujący pojazdem może używać tylnych świateł przeciwmgłowych, jeżeli zmniejszona przejrzystość powietrza ogranicza widoczność na odległość mniejszą niż 50 m.",
                    Points = 2
                },

                new Question()
                {
                    PhotoPath = "[68]Pytanie_mruganie.png", Text =
                        "Czy w dzień zabronione jest ostrzeganie światłami drogowymi, " +
                        "jeżeli może spowodować to oślepienie innych kierujących?",
                    IsHard = false,
                    CorrectDescription =
                        "Nie można używać świateł drogowych jeśli niesie to ryzyko oślepienia innych kierujących.",
                    Points = 2
                },

                new Question()
                {
                    PhotoPath = "[69]Pytanie_drogowee.png",
                    Text = "Czy w widocznej sytuacji kierujący może włączyć światła drogowe?", IsHard = false,
                    CorrectDescription =
                        "Używanie świateł drogowych w dzień byłoby uciążliwe zwłaszcza dla innych kierujących.",
                    Points = 2
                },

                new Question()
                {
                    PhotoPath = "[70]Pytanie_rownorzedne.png", Text =
                        "Czy jadąc na wprost na tym skrzyżowaniu, masz obowiązek ustąpić" +
                        " pierwszeństwa pojazdowi nadjeżdżającemu z lewej strony?",
                    IsHard = false, CorrectDescription = "Znajdujemy się z prawej strony nadjeżdżającego pojazdu.",
                    Points = 2
                },

                new Question()
                {
                    PhotoPath = "[71]Pytanie_rowno.png", Text =
                        "Czy jadąc z prędkością 50 km/h i mając niedostateczną widoczność po prawej stronie, " +
                        "powinieneś na tym skrzyżowaniu zmniejszyć prędkość?",
                    IsHard = false,
                    CorrectDescription =
                        "Kierujący pojazdem, zbliżając się do skrzyżowania, jest obowiązany zachować szczególną ostrożność i ustąpić pierwszeństwa pojazdowi nadjeżdżającemu z prawej strony.",
                    Points = 2
                },

                new Question()
                {
                    PhotoPath = "[72]Pytanie_sygnalizacja.png", Text =
                        "Zatrzymałeś właśnie swój pojazd. Czy w związku ze " +
                        "znaczeniem nadawanego sygnału świetlnego zrobiłeś to we właściwym miejscu?",
                    IsHard = false, CorrectDescription = "Można nieco bliżej.",
                    Points = 2
                },

                new Question()
                {
                    PhotoPath = "[73]Pytane_swietlna.png",
                    Text = "Czy w widocznej sytuacji z tego pasa ruchu możliwe jest tylko zawracanie na skrzyżowaniu?",
                    IsHard = false, CorrectDescription = "Sygnalizator umożliwia skręt w lewo oraz zawracanie.",
                    Points = 2
                },

                new Question()
                {
                    PhotoPath = "[74]Pytanie_stop.png",
                    Text = "Czy sygnał ten (czerwony i żółty nadawane jednocześnie) zezwala na wjazd za sygnalizator?",
                    IsHard = false, CorrectDescription = "Ten sygnał zobowiązuje do przygotowania się do ruszenia.",
                    Points = 2
                },

                new Question()
                {
                    PhotoPath = "[75]Pytanie_sygnalizacja.png",
                    Text = "Czy w przedstawionej sytuacji dozwolona jest dalsza jazda przez skrzyżowanie?",
                    IsHard = false, CorrectDescription = "Sygnał czerwony zabrania wjazdu za sygnalizator.", 
                    Points = 2
                },

                new Question()
                {
                    PhotoPath = "[76]Pytanie_swietlna.png",
                    Text = "Zamierzasz skręcić w prawo. Czy masz pierwszeństwo przed pieszymi?", IsHard = false,
                    CorrectDescription =
                        "Pieszy, przechodząc przez jezdnię lub torowisko, jest obowiązany zachować szczególną ostrożność oraz, korzystać z przejścia dla pieszych. Pieszy znajdujący się na tym przejściu ma pierwszeństwo przed pojazdem.",
                    Points = 2
                },

                new Question()
                {
                    PhotoPath = "[77]Pytanie_swiatlo.png", Text =
                        "Czy w przedstawionej sytuacji kierujący jest obowiązany " +
                        "zatrzymać pojazd przed jezdnią przy linii złożonej z trójkątów?",
                    IsHard = false, CorrectDescription = "Powinien się zatrzymać przy linii złożonej z prostokątów.",
                    Points = 2
                },

                new Question()
                {
                    PhotoPath = "[78]Pytanie_swiatla.png",
                    Text = "Czy w widocznej sytuacji dopuszczalne jest skręcanie w lewo i zawracanie na skrzyżowaniu?",
                    IsHard = false, CorrectDescription = "Sygnalizator umożliwia skręt w lewo oraz zawracanie.",
                    Points = 2
                },

                new Question()
                {
                    PhotoPath = "[79]Pytanie_szczalka.png",
                    Text = "Czy widoczny sygnał zezwala na skręcenie w prawo bez zatrzymania?",
                    IsHard = false,
                    CorrectDescription = "Sygnalizator S-2 zobowiązuje do zatrzymania.", 
                    Points = 2
                },

                new Question()
                {
                    PhotoPath = "[80]Pytanie_sztop.png",
                    Text = "Czy w tej sytuacji masz obowiązek zastosować się do znaku 'STOP'?", 
                    IsHard = false,
                    CorrectDescription =
                        "Wskazania sygnalizacji są ważniejsze od zasad ogólnych oraz znaków drogowych regulujących pierwszeństwem.",
                    Points = 2
                },

                new Question()
                {
                    PhotoPath = "[81]Pytanie_sygnalizacja.png",
                    Text =
                        "Czy na skrzyżowaniu o ruchu kierowanym sygnalizacją świetlną zabrania się wyprzedzania motorowerów i motocykli?",
                    IsHard = false, 
                    CorrectDescription = "Na skrzyżowaniu z sygnalizacją wyprzedzanie jest dozwolone.",
                    Points = 2
                },

                new Question()
                {
                    PhotoPath = "[82]Pytanie_swiatla.png",
                    Text =
                        "Czy w widocznej sytuacji z tego pasa ruchu dopuszczalne jest tylko skręcanie w lewo na skrzyżowaniu?",
                    IsHard = false, 
                    CorrectDescription = "Sygnalizator umożliwia skręt w lewo oraz zawracanie.",
                    Points = 2
                },

                new Question()
                {
                    PhotoPath = "[83]Pytanie_skret.png",
                    Text =
                        "Czy widoczny nad Twoim pasem ruchu sygnał świetlny zobowiązuje Cię do zatrzymania się przed sygnalizatorem?",
                    IsHard = false, 
                    CorrectDescription = "Jest to sygnał umożliwiający bezkolizyjny skręt w lewo.",
                    Points = 2
                },

                new Question()
                {
                    PhotoPath = "[84]S_Pytanie_pieszy.png",
                    Text = "Który z wymienionych czynników ma bezpośredni wpływ na drogę hamowania?",
                    IsHard = true,
                    CorrectDescription = "Prędkość pojazdu ma bezpośredni wpływ na drogę hamowania.", 
                    Points = 3
                },

                new Question()
                {
                    PhotoPath = "[85]S_Pytanie_hamowanie.png",
                    Text =
                        "Którą z wymienionych technik hamowania awaryjnego należy zastosować w samochodzie osobowym wyposażonym w układ ABS?",
                    IsHard = true,
                    CorrectDescription =
                        "ABS - układ stosowany w pojazdach mechanicznych w celu zapobiegania blokowaniu kół podczas hamowania.",
                    Points = 3
                },

                new Question()
                {
                    PhotoPath = "[86]S_Pytanie_motocykl.png",
                    Text = "Jaki odstęp należy zachować wyprzedzając motocyklistę?", 
                    IsHard = true,
                    CorrectDescription =
                        "Kierujący pojazdem jest obowiązany przy wyprzedzaniu zachować szczególną ostrożność, a zwłaszcza bezpieczny odstęp od wyprzedzanego pojazdu lub uczestnika ruchu. W razie wyprzedzania roweru, wózka rowerowego, motoroweru, motocykla lub kolumny pieszych odstęp ten nie może być mniejszy niż 1 m.",
                    Points = 3
                },

                new Question()
                {
                    PhotoPath = "[87]S_Pytanie_abs.png",
                    Text =
                        "W jaki sposób powinieneś hamować awaryjnie samochodem osobowym wyposażonym w układ przeciwblokujący (ABS)?",
                    IsHard = true,
                    CorrectDescription =
                        "ABS - układ stosowany w pojazdach mechanicznych w celu zapobiegania blokowaniu kół podczas hamowania.",
                    Points = 3
                },

                new Question()
                {
                    PhotoPath = "[88]S_Pytanie_odstep.png",
                    Text = "Jaki odstęp powinieneś zachować jadąc za innym pojazdem?", 
                    IsHard = true,
                    CorrectDescription =
                        "Kierujący powinien zachowywać odstęp niezbędny do uniknięcia zderzenia w razie hamowania lub zatrzymania się poprzedzającego pojazdu.",
                    Points = 3
                },

                new Question()
                {
                    PhotoPath = "[89]S_Pytanie_autostrada.png",
                    Text =
                        "Poruszasz się autostradą i zamierzasz ją opuścić. W którym miejscu rozpoczniesz hamowanie przed zjazdem z autostrady?",
                    IsHard = true,
                    CorrectDescription =
                        "Nie należy spowalniać ruchu pojazdów, które dalej będą poruszać się autostradą.",
                    Points = 3
                },

                new Question()
                {
                    PhotoPath = "[90]S_Pytanie_dziury.png",
                    Text = "Jaki odstęp musisz zachować od omijanej przeszkody?", IsHard = true,
                    CorrectDescription = "Odstęp w takich okolicznościach musi pozostać bezpieczny", 
                    Points = 3
                },

                new Question()
                {
                    PhotoPath = "[91]S_Pytanie_odstep.png",
                    Text =
                        "Jaki odstęp od poprzedzającego pojazdu musisz utrzymać stojąc przed skrzyżowaniem i czekając na sygnał zielony?",
                    IsHard = true, CorrectDescription = "Odstęp w takich okolicznościach musi pozostać bezpieczny.",
                    Points = 3
                },

                new Question()
                {
                    PhotoPath = "[92]S_Pytanie_dlugosc.png",
                    Text =
                        "Który z wymienionych czynników ma bezpośredni wpływ na długość całkowitej drogi zatrzymania samochodu osobowego?",
                    IsHard = true,
                    CorrectDescription =
                        "Bezpośredni wpływ na całkowitą drogę zatrzymania ma czas reakcji kierującego.",
                    Points = 3
                },

                new Question()
                {
                    PhotoPath = "[93]S_Pytanie_hamowanie.png",
                    Text = "Który z wymienionych czynników ma bezpośredni wpływ na drogę hamowania?", 
                    IsHard = true,
                    CorrectDescription = "Stan układu hamulcowego ma bezpośredni wpływ na drogę hamowania.", 
                    Points = 3
                },

                new Question()
                {
                    PhotoPath = "[94]S_Pytanie_pas.png",
                    Text = "Kiedy masz obowiązek zmienić pas na prawy po zakończeniu manewru wyprzedzania?",
                    IsHard = true,
                    CorrectDescription =
                        "Trzeba wrócić na prawy pas po upewnieniu się, że nie utrudni się ruchu kierującemu pojazdem wyprzedzanym.",
                    Points = 3
                },

                new Question()
                {
                    PhotoPath = "[96]S_Pytanie_wymijanie.png",
                    Text =
                        "Na jakie czynniki i okoliczności musisz zwrócić uwagę dobierając bezpieczną prędkość jazdy do panujących warunków drogowych?",
                    IsHard = true,
                    CorrectDescription =
                        "Kierujący pojazdem jest obowiązany: przy wymijaniu zachować bezpieczny odstęp od wymijanego pojazdu",
                    Points = 3
                },

                new Question()
                {
                    PhotoPath = "[97]S_Pytanie_hamowania.png",
                    Text = "Jak zmienia się droga hamowania pojazdu zależnie od wzrostu prędkości?", 
                    IsHard = true,
                    CorrectDescription = "Im wyższa prędkość tym dłuższa droga hamowania.", 
                    Points = 3
                },

                new Question()
                {
                    PhotoPath = "[98]S_Pytanie_rower.png",
                    Text = "Jaki odstęp musisz zachować wyprzedzając rower lub motorower?", 
                    IsHard = true,
                    CorrectDescription =
                        "W razie wyprzedzania roweru, wózka rowerowego motoroweru, motocykla lub kolumny pieszych odstęp nie może być mniejszy niż 1 m.",
                    Points = 3
                },

                new Question()
                {
                    PhotoPath = "[99]S_Pytanie_odleglosc.png",
                    Text =
                        "Który z wymienionych czynników ma decydujący wpływ na określanie odległości od pojazdu jadącego przed nami?",
                    IsHard = true,
                    CorrectDescription =
                        "Prędkość pojazdu ma decydujący wpływ na określanie odległości od pojazdu jadącego przed nami.",
                    Points = 3
                },

                new Question()
                {
                    PhotoPath = "[100]S_Pytanie_odstep.png",
                    Text =
                        "Od czego, w szczególności, zależy bezpieczny odstęp, który powinieneś zachować w tej sytuacji?",
                    IsHard = true,
                    CorrectDescription =
                        "Bezpieczny odstęp od poprzedzającego pojazdu uzależniony jest od prędkości z jaką się poruszamy.",
                    Points = 3
                }

                /*
                new Question() { PhotoPath = "nukk", Text = "", IsHard = false },
                */
            };
            context.Questions.AddRange(questions);

            IList<QuestionOption> questionOptions = new List<QuestionOption>()
            {
                new QuestionOption() {Question = questions.ElementAt(0), Text = "Tak", IsCorrect = false},
                new QuestionOption() {Question = questions.ElementAt(0), Text = "Nie", IsCorrect = true},

                new QuestionOption() {Question = questions.ElementAt(1), Text = "Tak", IsCorrect = true},
                new QuestionOption() {Question = questions.ElementAt(1), Text = "Nie", IsCorrect = false},

                new QuestionOption() {Question = questions.ElementAt(2), Text = "Tak", IsCorrect = true},
                new QuestionOption() {Question = questions.ElementAt(2), Text = "Nie", IsCorrect = false},

                new QuestionOption() {Question = questions.ElementAt(3), Text = "Tak", IsCorrect = false},
                new QuestionOption() {Question = questions.ElementAt(3), Text = "Nie", IsCorrect = true},

                new QuestionOption() {Question = questions.ElementAt(4), Text = "Tak", IsCorrect = true},
                new QuestionOption() {Question = questions.ElementAt(4), Text = "Nie", IsCorrect = false},

                new QuestionOption() {Question = questions.ElementAt(5), Text = "Tak", IsCorrect = true},
                new QuestionOption() {Question = questions.ElementAt(5), Text = "Nie", IsCorrect = false},

                new QuestionOption() {Question = questions.ElementAt(6), Text = "Tak", IsCorrect = false},
                new QuestionOption() {Question = questions.ElementAt(6), Text = "Nie", IsCorrect = true},

                new QuestionOption() {Question = questions.ElementAt(7), Text = "Tak", IsCorrect = false},
                new QuestionOption() {Question = questions.ElementAt(7), Text = "Nie", IsCorrect = true},

                new QuestionOption() {Question = questions.ElementAt(8), Text = "Tak", IsCorrect = true},
                new QuestionOption() {Question = questions.ElementAt(8), Text = "Nie", IsCorrect = false},

                new QuestionOption() {Question = questions.ElementAt(9), Text = "Tak", IsCorrect = true},
                new QuestionOption() {Question = questions.ElementAt(9), Text = "Nie", IsCorrect = false},

                new QuestionOption() {Question = questions.ElementAt(10), Text = "Tak", IsCorrect = false},
                new QuestionOption() {Question = questions.ElementAt(10), Text = "Nie", IsCorrect = true},

                new QuestionOption() {Question = questions.ElementAt(11), Text = "Tak", IsCorrect = true},
                new QuestionOption() {Question = questions.ElementAt(11), Text = "Nie", IsCorrect = false},

                new QuestionOption() {Question = questions.ElementAt(12), Text = "Tak", IsCorrect = true},
                new QuestionOption() {Question = questions.ElementAt(12), Text = "Nie", IsCorrect = false},

                new QuestionOption() {Question = questions.ElementAt(13), Text = "Tak", IsCorrect = true},
                new QuestionOption() {Question = questions.ElementAt(13), Text = "Nie", IsCorrect = false},

                new QuestionOption() {Question = questions.ElementAt(14), Text = "Tak", IsCorrect = true},
                new QuestionOption() {Question = questions.ElementAt(14), Text = "Nie", IsCorrect = false},

                new QuestionOption() {Question = questions.ElementAt(15), Text = "Tak", IsCorrect = true},
                new QuestionOption() {Question = questions.ElementAt(15), Text = "Nie", IsCorrect = false},

                new QuestionOption() {Question = questions.ElementAt(16), Text = "Tak", IsCorrect = false},
                new QuestionOption() {Question = questions.ElementAt(16), Text = "Nie", IsCorrect = true},

                new QuestionOption() {Question = questions.ElementAt(17), Text = "Tak", IsCorrect = true},
                new QuestionOption() {Question = questions.ElementAt(17), Text = "Nie", IsCorrect = false},

                new QuestionOption() {Question = questions.ElementAt(18), Text = "Tak", IsCorrect = true},
                new QuestionOption() {Question = questions.ElementAt(18), Text = "Nie", IsCorrect = false},

                new QuestionOption() {Question = questions.ElementAt(19), Text = "Tak", IsCorrect = false},
                new QuestionOption() {Question = questions.ElementAt(19), Text = "Nie", IsCorrect = true},

                new QuestionOption() {Question = questions.ElementAt(20), Text = "Tak", IsCorrect = false},
                new QuestionOption() {Question = questions.ElementAt(20), Text = "Nie", IsCorrect = true},

                new QuestionOption() {Question = questions.ElementAt(21), Text = "Tak", IsCorrect = true},
                new QuestionOption() {Question = questions.ElementAt(21), Text = "Nie", IsCorrect = false},

                new QuestionOption() {Question = questions.ElementAt(22), Text = "Tak", IsCorrect = true},
                new QuestionOption() {Question = questions.ElementAt(22), Text = "Nie", IsCorrect = false},

                new QuestionOption() {Question = questions.ElementAt(23), Text = "Tak", IsCorrect = false},
                new QuestionOption() {Question = questions.ElementAt(23), Text = "Nie", IsCorrect = true},

                new QuestionOption() {Question = questions.ElementAt(24), Text = "Tak", IsCorrect = true},
                new QuestionOption() {Question = questions.ElementAt(24), Text = "Nie", IsCorrect = false},

                new QuestionOption() {Question = questions.ElementAt(25), Text = "Tak", IsCorrect = true},
                new QuestionOption() {Question = questions.ElementAt(25), Text = "Nie", IsCorrect = false},

                new QuestionOption() {Question = questions.ElementAt(26), Text = "Tak", IsCorrect = false},
                new QuestionOption() {Question = questions.ElementAt(26), Text = "Nie", IsCorrect = true},

                new QuestionOption() {Question = questions.ElementAt(27), Text = "Tak", IsCorrect = false},
                new QuestionOption() {Question = questions.ElementAt(27), Text = "Nie", IsCorrect = true},

                new QuestionOption() {Question = questions.ElementAt(28), Text = "Tak", IsCorrect = false},
                new QuestionOption() {Question = questions.ElementAt(28), Text = "Nie", IsCorrect = true},

                new QuestionOption() {Question = questions.ElementAt(29), Text = "Tak", IsCorrect = true},
                new QuestionOption() {Question = questions.ElementAt(29), Text = "Nie", IsCorrect = false},

                new QuestionOption() {Question = questions.ElementAt(30), Text = "Tak", IsCorrect = false},
                new QuestionOption() {Question = questions.ElementAt(30), Text = "Nie", IsCorrect = true},

                new QuestionOption() {Question = questions.ElementAt(31), Text = "Tak", IsCorrect = true},
                new QuestionOption() {Question = questions.ElementAt(31), Text = "Nie", IsCorrect = false},

                new QuestionOption() {Question = questions.ElementAt(32), Text = "Tak", IsCorrect = true},
                new QuestionOption() {Question = questions.ElementAt(32), Text = "Nie", IsCorrect = false},

                new QuestionOption() {Question = questions.ElementAt(33), Text = "Tak", IsCorrect = true},
                new QuestionOption() {Question = questions.ElementAt(33), Text = "Nie", IsCorrect = false},

                new QuestionOption() {Question = questions.ElementAt(34), Text = "Tak", IsCorrect = true},
                new QuestionOption() {Question = questions.ElementAt(34), Text = "Nie", IsCorrect = false},

                new QuestionOption() {Question = questions.ElementAt(35), Text = "Tak", IsCorrect = false},
                new QuestionOption() {Question = questions.ElementAt(35), Text = "Nie", IsCorrect = true},

                new QuestionOption() {Question = questions.ElementAt(36), Text = "Tak", IsCorrect = false},
                new QuestionOption() {Question = questions.ElementAt(36), Text = "Nie", IsCorrect = true},

                new QuestionOption() {Question = questions.ElementAt(37), Text = "Tak", IsCorrect = false},
                new QuestionOption() {Question = questions.ElementAt(37), Text = "Nie", IsCorrect = true},

                new QuestionOption() {Question = questions.ElementAt(38), Text = "Tak", IsCorrect = false},
                new QuestionOption() {Question = questions.ElementAt(38), Text = "Nie", IsCorrect = true},

                new QuestionOption() {Question = questions.ElementAt(39), Text = "Tak", IsCorrect = true},
                new QuestionOption() {Question = questions.ElementAt(39), Text = "Nie", IsCorrect = false},

                new QuestionOption() {Question = questions.ElementAt(40), Text = "Tak", IsCorrect = true},
                new QuestionOption() {Question = questions.ElementAt(40), Text = "Nie", IsCorrect = false},

                new QuestionOption() {Question = questions.ElementAt(41), Text = "120 km/h", IsCorrect = false},
                new QuestionOption() {Question = questions.ElementAt(41), Text = "130 km/h", IsCorrect = false},
                new QuestionOption() {Question = questions.ElementAt(41), Text = "140 km/h", IsCorrect = true},

                new QuestionOption()
                    {Question = questions.ElementAt(42), Text = "Co najmniej 40 km/h.", IsCorrect = false},
                new QuestionOption()
                    {Question = questions.ElementAt(42), Text = "Co najmniej 60 km/h.", IsCorrect = false},
                new QuestionOption()
                {
                    Question = questions.ElementAt(42), Text = "Bezpieczną, nieutrudniającą jazdy innym kierującym, " +
                                                               "jednak nie większą niż dopuszczalna.",
                    IsCorrect = true
                },

                new QuestionOption() {Question = questions.ElementAt(43), Text = "90 km/h", IsCorrect = false},
                new QuestionOption() {Question = questions.ElementAt(43), Text = "100 km/h", IsCorrect = true},
                new QuestionOption() {Question = questions.ElementAt(43), Text = "110 km/h", IsCorrect = false},

                new QuestionOption() {Question = questions.ElementAt(44), Text = "60 km/h", IsCorrect = true},
                new QuestionOption() {Question = questions.ElementAt(44), Text = "50 km/h", IsCorrect = false},
                new QuestionOption() {Question = questions.ElementAt(44), Text = "70 km/h", IsCorrect = false},

                new QuestionOption() {Question = questions.ElementAt(45), Text = "70 km/h", IsCorrect = false},
                new QuestionOption() {Question = questions.ElementAt(45), Text = "100 km/h", IsCorrect = false},
                new QuestionOption() {Question = questions.ElementAt(45), Text = "90 km/h", IsCorrect = true},

                new QuestionOption()
                    {Question = questions.ElementAt(46), Text = "50 km/h - w godzinach 5.00-23.00", IsCorrect = false},
                new QuestionOption()
                    {Question = questions.ElementAt(46), Text = "50 km/h - przez całą dobę", IsCorrect = false},
                new QuestionOption()
                    {Question = questions.ElementAt(46), Text = "60 km/h - przez całą dobę", IsCorrect = true},

                new QuestionOption() {Question = questions.ElementAt(47), Text = "90 km/h", IsCorrect = true},
                new QuestionOption() {Question = questions.ElementAt(47), Text = "100 km/h", IsCorrect = false},
                new QuestionOption() {Question = questions.ElementAt(47), Text = "110 km/h", IsCorrect = false},

                new QuestionOption() {Question = questions.ElementAt(48), Text = "70 km/h", IsCorrect = false},
                new QuestionOption() {Question = questions.ElementAt(48), Text = "80 km/h", IsCorrect = true},
                new QuestionOption() {Question = questions.ElementAt(48), Text = "100 km/h", IsCorrect = false},

                new QuestionOption() {Question = questions.ElementAt(49), Text = "90 km/h", IsCorrect = false},
                new QuestionOption() {Question = questions.ElementAt(49), Text = "100 km/h", IsCorrect = true},
                new QuestionOption() {Question = questions.ElementAt(49), Text = "120 km/h", IsCorrect = false},

                new QuestionOption() {Question = questions.ElementAt(50), Text = "30 km/h", IsCorrect = true},
                new QuestionOption() {Question = questions.ElementAt(50), Text = "40 km/h", IsCorrect = false},
                new QuestionOption()
                {
                    Question = questions.ElementAt(50), Text = "50 km/h w godzinach 5.00-23.00 " +
                                                               "i 60 km/h w godzinach 23.00-5.00",
                    IsCorrect = false
                },

                new QuestionOption()
                {
                    Question = questions.ElementAt(51), Text = "Nie, gdy poruszam się z niewielką prędkością.",
                    IsCorrect = false
                },
                new QuestionOption()
                    {Question = questions.ElementAt(51), Text = "Tak, w każdym przypadku.", IsCorrect = true},
                new QuestionOption()
                {
                    Question = questions.ElementAt(51),
                    Text = "Tak, ale tylko w przypadku jazdy z prędkością większą niż 50 km/h.", IsCorrect = false
                },

                new QuestionOption()
                {
                    Question = questions.ElementAt(52),
                    Text = "Zmniejsza ryzyko powstania obrażeń ciężkich lub śmiertelnych.", IsCorrect = true
                },
                new QuestionOption()
                {
                    Question = questions.ElementAt(52), Text = "Zwiększa ryzyko powstania obrażeń lekkich.",
                    IsCorrect = false
                },
                new QuestionOption()
                {
                    Question = questions.ElementAt(52),
                    Text = "Nie ma wpływu na ryzyko powstania obrażeń bez względu na ich rodzaj.", IsCorrect = false
                },

                new QuestionOption() {Question = questions.ElementAt(53), Text = "Nie", IsCorrect = false},
                new QuestionOption()
                {
                    Question = questions.ElementAt(53),
                    Text = "Tak, ale tylko wówczas, gdy pojazd porusza się poza obszarem zabudowanym.",
                    IsCorrect = false
                },
                new QuestionOption() {Question = questions.ElementAt(53), Text = "Tak", IsCorrect = true},

                new QuestionOption()
                {
                    Question = questions.ElementAt(54), Text = "Oznaczanie ładunku na bagażniku dachowym pojazdu.",
                    IsCorrect = false
                },
                new QuestionOption()
                {
                    Question = questions.ElementAt(54),
                    Text = "Ostrzeganie o unieruchomieniu pojazdu na drodze z powodu awarii.", IsCorrect = true
                },
                new QuestionOption()
                {
                    Question = questions.ElementAt(54),
                    Text = "Oznaczanie niesprawnych tylnych świateł pozycyjnych poruszającego się pojazdu.",
                    IsCorrect = false
                },

                new QuestionOption()
                    {Question = questions.ElementAt(55), Text = "Skręcenie lub zwinięcie pasów.", IsCorrect = false},
                new QuestionOption()
                {
                    Question = questions.ElementAt(55), Text = "Przetarcia, przewężenia i naderwania taśmy pasów.",
                    IsCorrect = true
                },
                new QuestionOption()
                {
                    Question = questions.ElementAt(55), Text = "Trwałe zabrudzenia i plamy na taśmie pasów.",
                    IsCorrect = false
                },

                new QuestionOption()
                {
                    Question = questions.ElementAt(56), Text = "Każdy pasażer na tylnym siedzeniu pojazdu.",
                    IsCorrect = false
                },
                new QuestionOption()
                {
                    Question = questions.ElementAt(56), Text = "Każdy pasażer w trakcie spożywania posiłku.",
                    IsCorrect = false
                },
                new QuestionOption()
                    {Question = questions.ElementAt(56), Text = "Kobieta w widocznej ciąży.", IsCorrect = true},

                new QuestionOption()
                {
                    Question = questions.ElementAt(57),
                    Text = "Tak, aby jego górna część znajdowała się na wysokości czubka głowy.", IsCorrect = true
                },
                new QuestionOption()
                {
                    Question = questions.ElementAt(57),
                    Text = "Tak, aby jego górna część znajdowała się powyżej czubka głowy.", IsCorrect = false
                },
                new QuestionOption()
                {
                    Question = questions.ElementAt(57),
                    Text = "Tak, aby jego górna część znajdowała się na wysokości szyi.", IsCorrect = false
                },

                new QuestionOption() {Question = questions.ElementAt(58), Text = "A", IsCorrect = true},
                new QuestionOption() {Question = questions.ElementAt(58), Text = "B", IsCorrect = false},
                new QuestionOption() {Question = questions.ElementAt(58), Text = "C", IsCorrect = false},

                new QuestionOption() {Question = questions.ElementAt(59), Text = "Tak", IsCorrect = true},
                new QuestionOption() {Question = questions.ElementAt(59), Text = "Nie", IsCorrect = false},

                new QuestionOption() {Question = questions.ElementAt(60), Text = "Tak", IsCorrect = true},
                new QuestionOption() {Question = questions.ElementAt(60), Text = "Nie", IsCorrect = false},

                new QuestionOption() {Question = questions.ElementAt(61), Text = "Tak", IsCorrect = true},
                new QuestionOption() {Question = questions.ElementAt(61), Text = "Nie", IsCorrect = false},

                new QuestionOption() {Question = questions.ElementAt(62), Text = "Tak", IsCorrect = true},
                new QuestionOption() {Question = questions.ElementAt(62), Text = "Nie", IsCorrect = false},

                new QuestionOption() {Question = questions.ElementAt(63), Text = "Tak", IsCorrect = true},
                new QuestionOption() {Question = questions.ElementAt(63), Text = "Nie", IsCorrect = false},

                new QuestionOption() {Question = questions.ElementAt(64), Text = "Tak", IsCorrect = true},
                new QuestionOption() {Question = questions.ElementAt(64), Text = "Nie", IsCorrect = false},

                new QuestionOption() {Question = questions.ElementAt(65), Text = "Tak", IsCorrect = true},
                new QuestionOption() {Question = questions.ElementAt(65), Text = "Nie", IsCorrect = false},

                new QuestionOption() {Question = questions.ElementAt(66), Text = "Tak", IsCorrect = true},
                new QuestionOption() {Question = questions.ElementAt(66), Text = "Nie", IsCorrect = false},

                new QuestionOption() {Question = questions.ElementAt(67), Text = "Tak", IsCorrect = true},
                new QuestionOption() {Question = questions.ElementAt(67), Text = "Nie", IsCorrect = false},

                new QuestionOption() {Question = questions.ElementAt(68), Text = "Tak", IsCorrect = true},
                new QuestionOption() {Question = questions.ElementAt(68), Text = "Nie", IsCorrect = false},

                new QuestionOption() {Question = questions.ElementAt(69), Text = "Tak", IsCorrect = false},
                new QuestionOption() {Question = questions.ElementAt(69), Text = "Nie", IsCorrect = true},

                new QuestionOption() {Question = questions.ElementAt(70), Text = "Tak", IsCorrect = false},
                new QuestionOption() {Question = questions.ElementAt(70), Text = "Nie", IsCorrect = true},

                new QuestionOption() {Question = questions.ElementAt(71), Text = "Tak", IsCorrect = true},
                new QuestionOption() {Question = questions.ElementAt(71), Text = "Nie", IsCorrect = false},

                new QuestionOption() {Question = questions.ElementAt(72), Text = "Tak", IsCorrect = false},
                new QuestionOption() {Question = questions.ElementAt(72), Text = "Nie", IsCorrect = true},

                new QuestionOption() {Question = questions.ElementAt(73), Text = "Tak", IsCorrect = false},
                new QuestionOption() {Question = questions.ElementAt(73), Text = "Nie", IsCorrect = true},

                new QuestionOption() {Question = questions.ElementAt(74), Text = "Tak", IsCorrect = false},
                new QuestionOption() {Question = questions.ElementAt(74), Text = "Nie", IsCorrect = true},

                new QuestionOption() {Question = questions.ElementAt(75), Text = "Tak", IsCorrect = false},
                new QuestionOption() {Question = questions.ElementAt(75), Text = "Nie", IsCorrect = true},

                new QuestionOption() {Question = questions.ElementAt(76), Text = "Tak", IsCorrect = false},
                new QuestionOption() {Question = questions.ElementAt(76), Text = "Nie", IsCorrect = true},

                new QuestionOption() {Question = questions.ElementAt(77), Text = "Tak", IsCorrect = false},
                new QuestionOption() {Question = questions.ElementAt(77), Text = "Nie", IsCorrect = true},

                new QuestionOption() {Question = questions.ElementAt(78), Text = "Tak", IsCorrect = true},
                new QuestionOption() {Question = questions.ElementAt(78), Text = "Nie", IsCorrect = false},

                new QuestionOption() {Question = questions.ElementAt(79), Text = "Tak", IsCorrect = false},
                new QuestionOption() {Question = questions.ElementAt(79), Text = "Nie", IsCorrect = true},

                new QuestionOption() {Question = questions.ElementAt(80), Text = "Tak", IsCorrect = false},
                new QuestionOption() {Question = questions.ElementAt(80), Text = "Nie", IsCorrect = true},

                new QuestionOption() {Question = questions.ElementAt(81), Text = "Tak", IsCorrect = false},
                new QuestionOption() {Question = questions.ElementAt(81), Text = "Nie", IsCorrect = true},

                new QuestionOption() {Question = questions.ElementAt(82), Text = "Tak", IsCorrect = false},
                new QuestionOption() {Question = questions.ElementAt(82), Text = "Nie", IsCorrect = true},

                new QuestionOption() {Question = questions.ElementAt(83), Text = "Tak", IsCorrect = false},
                new QuestionOption() {Question = questions.ElementAt(83), Text = "Nie", IsCorrect = true},

                new QuestionOption() {Question = questions.ElementAt(84), Text = "Prędkość jazdy.", IsCorrect = true},
                new QuestionOption()
                    {Question = questions.ElementAt(84), Text = "Oznakowanie poziome.", IsCorrect = false},
                new QuestionOption()
                    {Question = questions.ElementAt(84), Text = "Widoczność na drodze.", IsCorrect = false},

                new QuestionOption()
                {
                    Question = questions.ElementAt(85), Text = "Szybko i mocno wcisnąć pedał hamulca.", IsCorrect = true
                },
                new QuestionOption()
                {
                    Question = questions.ElementAt(85),
                    Text = "Delikatnie wcisnąć pedał hamulca, stopniowo zwiększając nacisk, aż zadziała układ ABS.",
                    IsCorrect = false
                },
                new QuestionOption()
                    {Question = questions.ElementAt(85), Text = "Hamować pulsacyjnie.", IsCorrect = false},

                new QuestionOption()
                    {Question = questions.ElementAt(86), Text = "Bezpieczny, mniejszy niż 1 metr.", IsCorrect = false},
                new QuestionOption()
                    {Question = questions.ElementAt(86), Text = "Każdy odstęp, ale bezpieczny.", IsCorrect = false},
                new QuestionOption()
                    {Question = questions.ElementAt(86), Text = "Nie mniejszy niż 1 metr.", IsCorrect = true},

                new QuestionOption()
                {
                    Question = questions.ElementAt(87),
                    Text = "Mocno wcisnąć pedał hamulca i nie zwalniać go w chwili zadziałania układu ABS.",
                    IsCorrect = true
                },
                new QuestionOption()
                {
                    Question = questions.ElementAt(87),
                    Text = "Mocno wcisnąć pedał hamulca i zmniejszyć nacisk w chwili zadziałania układu ABS.",
                    IsCorrect = false
                },
                new QuestionOption()
                {
                    Question = questions.ElementAt(87), Text = "Hamować pulsacyjnie aż do zadziałania układu ABS.",
                    IsCorrect = false
                },

                new QuestionOption()
                {
                    Question = questions.ElementAt(88),
                    Text = "Uzależniony wyłącznie od twoich umiejętności i doświadczenia.", IsCorrect = false
                },
                new QuestionOption()
                {
                    Question = questions.ElementAt(88),
                    Text =
                        "Niezbędny do uniknięcia zderzenia w razie hamowania lub zatrzymania się poprzedzającego pojazdu.",
                    IsCorrect = true
                },
                new QuestionOption()
                {
                    Question = questions.ElementAt(88),
                    Text =
                        "Wynoszący co najmniej dwie długości twojego pojazdu niezależnie od prędkości i warunków panujących na drodze.",
                    IsCorrect = false
                },

                new QuestionOption()
                {
                    Question = questions.ElementAt(89), Text = "Przed wjazdem na pas wyłączenia (zjazdu).",
                    IsCorrect = false
                },
                new QuestionOption()
                {
                    Question = questions.ElementAt(89), Text = "Po wjeździe na początek pasa wyłączenia (zjazdu).",
                    IsCorrect = true
                },
                new QuestionOption()
                {
                    Question = questions.ElementAt(89), Text = "W dowolnym miejscu na autostradzie.", IsCorrect = false
                },

                new QuestionOption() {Question = questions.ElementAt(90), Text = "1 m.", IsCorrect = false},
                new QuestionOption() {Question = questions.ElementAt(90), Text = "Bezpieczny.", IsCorrect = true},
                new QuestionOption() {Question = questions.ElementAt(90), Text = "1.5 m.", IsCorrect = false},

                new QuestionOption()
                    {Question = questions.ElementAt(91), Text = "Minimalnie 0.5 m.", IsCorrect = false},
                new QuestionOption()
                    {Question = questions.ElementAt(91), Text = "Bezpieczny odstęp.", IsCorrect = true},
                new QuestionOption()
                    {Question = questions.ElementAt(91), Text = "Maksymalnie 3 m, minimalnie 1m.", IsCorrect = false},

                new QuestionOption()
                    {Question = questions.ElementAt(92), Text = "Ilość paliwa w zbiorniku.", IsCorrect = false},
                new QuestionOption()
                {
                    Question = questions.ElementAt(92), Text = "Stan układu kierowniczego pojazdu.", IsCorrect = false
                },
                new QuestionOption()
                    {Question = questions.ElementAt(92), Text = "Czas reakcji kierującego pojazdem.", IsCorrect = true},

                new QuestionOption()
                    {Question = questions.ElementAt(93), Text = "Oznakowanie poziome.", IsCorrect = false},
                new QuestionOption() {Question = questions.ElementAt(93), Text = "Stan hamulców.", IsCorrect = true},
                new QuestionOption()
                    {Question = questions.ElementAt(93), Text = "Oznakowanie pionowe.", IsCorrect = false},

                new QuestionOption()
                {
                    Question = questions.ElementAt(94),
                    Text = "Gdy widzę tył pojazdu wyprzedzanego w prawym lusterku wstecznym.", IsCorrect = false
                },
                new QuestionOption()
                {
                    Question = questions.ElementAt(94), Text = "Gdy wyprzedzany pojazd jest 100 metrów za mną.",
                    IsCorrect = false
                },
                new QuestionOption()
                {
                    Question = questions.ElementAt(94),
                    Text = "Gdy upewnię się, że nie ma ryzyka 'zajechania drogi' wyprzedzanemu pojazdowi.",
                    IsCorrect = true
                },

                new QuestionOption()
                {
                    Question = questions.ElementAt(95), Text = "Na prawidłowe ułożenie rąk na kierownicy.",
                    IsCorrect = false
                },
                new QuestionOption()
                    {Question = questions.ElementAt(95), Text = "Na pojazdy jadące za tobą.", IsCorrect = false},
                new QuestionOption()
                {
                    Question = questions.ElementAt(95), Text = "Na zachowanie bezpiecznego odstępu.", IsCorrect = true
                },

                new QuestionOption()
                {
                    Question = questions.ElementAt(96),
                    Text = "Droga hamowania wydłuża się wraz ze wzrostem prędkości.", IsCorrect = true
                },
                new QuestionOption()
                {
                    Question = questions.ElementAt(96),
                    Text = "Droga hamowania ulega skróceniu wraz ze wzrostem prędkości.", IsCorrect = false
                },
                new QuestionOption()
                {
                    Question = questions.ElementAt(96), Text = "Prędkość jazdy nie ma wpływu na drogę hamowania.",
                    IsCorrect = false
                },

                new QuestionOption()
                    {Question = questions.ElementAt(97), Text = "Nie mniejszy niż 1 metr.", IsCorrect = true},
                new QuestionOption()
                    {Question = questions.ElementAt(97), Text = "Nie mniejszy niż 0.5 metra.", IsCorrect = false},
                new QuestionOption()
                    {Question = questions.ElementAt(97), Text = "Dowolny, ale bezpieczny.", IsCorrect = false},

                new QuestionOption() {Question = questions.ElementAt(98), Text = "Długość pojazdu.", IsCorrect = false},
                new QuestionOption()
                    {Question = questions.ElementAt(98), Text = "Szerokość pojazdu.", IsCorrect = false},
                new QuestionOption() {Question = questions.ElementAt(98), Text = "Prędkość pojazdu.", IsCorrect = true},

                new QuestionOption()
                    {Question = questions.ElementAt(99), Text = "Od prędkości, z jaką jedziesz.", IsCorrect = true},
                new QuestionOption()
                    {Question = questions.ElementAt(99), Text = "Od mocy silnika w pojeździe.", IsCorrect = false},
                new QuestionOption()
                    {Question = questions.ElementAt(99), Text = "Od szerokości jezdni.", IsCorrect = false}
            };

            context.QuestionOptions.AddRange(questionOptions);

            base.Seed(context);
        }
    }
}