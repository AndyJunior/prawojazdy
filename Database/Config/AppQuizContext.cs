﻿using System.Data.Entity;
using Database.Model;

namespace Database.Config
{
    public class AppQuizContext : DbContext
    {
        public AppQuizContext() : base("PrawoJazdy")
        {
            System.Data.Entity.Database.SetInitializer<AppQuizContext>(new AppInitializer());
        }
        public DbSet<Question> Questions { get; set; }

        public DbSet<QuestionOption> QuestionOptions { get; set; }

        public DbSet<Answer> Answers { get; set; }

    }
}
